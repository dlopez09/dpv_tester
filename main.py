from kivy.app import App
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager


class SideScreenManager(ScreenManager):
    pass

class Connection(Screen):
    pass


class TestBuildScreen(Screen):
    pass


class TestProgressScreen(Screen):
    pass


class NotSuccessConnect(Popup):
    pass


class ModuleSelect(BoxLayout):

    def __init__(self, **kwargs):
        super(ModuleSelect, self).__init__(**kwargs)


class TesterGUIApp(App):
    title = "DPV Tester App"

    def build(self):
        return ModuleSelect()


connect_color = "0,1,0,1"
disconnect_color = "1,0,0,1"

TesterGUI = TesterGUIApp()

TesterGUI.run()

