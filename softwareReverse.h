void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()){
    serInput = Serial.read();
    switch (serInput){
      case 0:                            //ZHOME
        digitalWrite(in4, HIGH);
        delay(100);
        digitalWrite(in4, LOW);
        Serial.write("zzzz");
        break;
      case 33:                           //STOP
        if(stop_state == 0){
          stop_state = 1;
          digitalWrite(in6, HIGH);
          Serial.write("stop");
        }
        else {
          stop_state = 0;
          digitalWrite(in6, LOW);
          Serial.write("ress");
        }
        break;
      case 35:                          //RV-JOG
        if(rvjog == 0){
          rvjog = 1;
          digitalWrite(in9, HIGH);
          Serial.write("rvjg");
        }
        else {
          rvjog = 0;
          digitalWrite(in9, LOW);
          Serial.write("strj");
        }
        break;
      case 36:                          //FW-JOG
        if(fwjog == 0){
          fwjog = 1;
          digitalWrite(in8, HIGH);
          Serial.write("fwjg");
        }
        else {
          fwjog = 0;
          digitalWrite(in8, LOW);
          Serial.write("stfj");
        }
        break;
      case 37:                          //ALM-RST
        digitalWrite(in7, HIGH);
        delay(100);
        digitalWrite(in7, LOW);
        Serial.write("arst");
        break;
      case 38:                          //PLS-RDY
        if(digitalRead(out2) == HIGH){
          Serial.write("plsr");
        }
        else{
          Serial.write("plnr");
        }
        break;
      case 42:                          //MOVE
        while(digitalRead(out4) == HIGH){
          Serial.write("mmmm");
        }
        Serial.write("fmmm");
        break;
      case 43:                          //CW +
        digitalWrite(cw_p, HIGH);
        delay(10);
        digitalWrite(cw_p, LOW);
        break;
      case 45:                          //CCW +
        digitalWrite(ccw_p, HIGH);
        delay(10);
        digitalWrite(ccw_p, LOW);
        break;
      case 61:                          //FREE
        if(free_state == 0){
          free_state = 1;
          digitalWrite(in5, HIGH);
          Serial.write("free");
        }
        else {
          free_state = 0;
          digitalWrite(in5, LOW);
          Serial.write("ntfr");
        }
        break;
      case 62:                           //READY
        if(digitalRead(out3) == HIGH){
          Serial.write("rrdy");
        }
        else{
          Serial.write("nrdy");
        }
        break;
      case 63:                          //IN-POS
        if(digitalRead(out1) == HIGH){
          Serial.write("inpo");
        }
        else{
          Serial.write("ninp");
        }
        break;
      case 64:                          //HOME-END
        if(digitalRead(out4) == HIGH){
          Serial.write("wait");
          break;
        }
        else if(digitalRead(out0) == HIGH){
          Serial.write("HOME");
          break;
        }
        else{
          Serial.write("errr");
          break;
        }
      case 65:                         //ALM-B
        if(digitalRead(out5) == HIGH){
          Serial.write("alrm");
        }
        else{
          Serial.write("cont");
        }
        break;
      default:
        Serial.write("Command Not Found");
    }
      
    
   
  }
}