int cw_p = 1;    // Clockwise Pulses
int ccw_p = 2;   // Couter Clockwise Pulses
int in4 = 3;     // ZHOME Input Turn ON to zhome  pg.39
int in6 = 4;     // STOP input turn on to stop motor, will be stopped for duration of input.  add code to disable pulsing if sent STOP
int in8 = 7;     // FW-JOG   will JOG fwd as long as input is ON
int in5 = 5;    // FREE when input is turned ON, the motor and brake will release.  DO NOT SEND FREE (use this input if necessary?)
int in7 = 8;    // ALM-RST input will reset alarm when ON.  B4 reset: turn OFF pulse input, remove cause of alarm
int in9 = 6;   // RV-JOG will JOG RV as long as input is ON

int out1 = 18;   // IN-POS output will turn ON when the motor has completed its movement
int out3 = 16;   // READY output turns ON when the driver is ready to execute operation
int out5 = 14;   // ALM-B output will turn OFF and motor will stop when alarm generates
int out0 = 21;    // HOME-END output will turn ON when H-S return-to-home op is complete
int out2 = 19;    // PLS-RDY output will turn ON when driver is ready to execute operation by inputting pulses
int out4 = 23;    // MOVE output will turn ON when motor is operating

int serInput = 0;
char pulseInput[10];
char endMark = '\n';
char rc;
int stop_state = 0;
int fwjog = 0;
int rvjog = 0;
int free_state = 0;
int cwpt = 0;
int ccwpt = 0;
String strInput;
int intInput;

byte zhome = 0;
byte stp = 33;
byte rvjg = 35;
byte fwjg = 36;
byte almrst = 37;
byte plsrdy = 38;
byte mve = 42;
byte cwp = 43;
byte ccwp = 45;
byte fre = 61;
byte rdy = 62;
byte inpos = 63;
byte hmend = 64;
byte almb = 65;


void setup() {
  // put your setup code here, to run once:
  // put your setup code here, to run once:
  pinMode(cw_p, OUTPUT);   //CW+, 43, 10ms delay
  pinMode(ccw_p, OUTPUT);  //CCW+, 45, 10ms delay
  pinMode(in4, OUTPUT);    //ZHOME, 0
  pinMode(in6, OUTPUT);    //STOP, 33, toggle
  pinMode(in8, OUTPUT);    //FW-JOG, 36, toggle
  pinMode(in5, OUTPUT);    //FREE, 61
  pinMode(in7, OUTPUT);    //ALM-RST, 37 
  pinMode(in9, OUTPUT);    //RV-JOG, 35, toggle
  
  pinMode(out0, INPUT);    //HOME-END, 64
  pinMode(out2, INPUT);    //PLS-RDY, 38
  pinMode(out4, INPUT);    //MOVE, 42
  pinMode(out1, INPUT);    //IN-POS, 63
  pinMode(out3, INPUT);    //READY, 62
  pinMode(out5, INPUT);    //ALM-B, 65

// Software reverse state
  digitalWrite(in8, HIGH);
  digitalWrite(in4, HIGH);
  digitalWrite(in5, HIGH);
  digitalWrite(in9, HIGH);
  digitalWrite(in7, HIGH);
  digitalWrite(in6, HIGH);
  digitalWrite(cw_p, HIGH);
  digitalWrite(ccw_p, HIGH);
  Serial.begin(115200);
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  // put your main code here, to run repeatedly:
  if(Serial.available()){
    serInput = Serial.read();
    switch (serInput){
      case 'I':
        digitalWrite(cw_p, HIGH);
        digitalWrite(ccw_p, HIGH);
        digitalWrite(in4, HIGH);
        digitalWrite(in6, HIGH);
        digitalWrite(in9, HIGH);
        digitalWrite(in8, HIGH);
        digitalWrite(in7, HIGH);
        digitalWrite(in5, HIGH);
        break;
      case 'm':                        //3.32
        digitalWrite(cw_p, LOW);
        delayMicroseconds(2075);
        digitalWrite(cw_p, HIGH);
        break;
      case 'n':                        //3.32
        digitalWrite(ccw_p, LOW);
        delayMicroseconds(2075);
        digitalWrite(ccw_p, HIGH);
        break;
      case 'o':                        //4.81x2
        digitalWrite(cw_p, LOW);
        delayMicroseconds(520);
        digitalWrite(cw_p, HIGH);
        delayMicroseconds(520);
        digitalWrite(cw_p, LOW);
        delayMicroseconds(520);
        digitalWrite(cw_p, HIGH);
        delayMicroseconds(520);
        break;
      case 'p':                        //4.81x2
        digitalWrite(ccw_p, LOW);
        delayMicroseconds(520);
        digitalWrite(ccw_p, HIGH);
        delayMicroseconds(520);
        digitalWrite(ccw_p, LOW);
        delayMicroseconds(520);
        digitalWrite(ccw_p, HIGH);
        delayMicroseconds(520);
        break;
      case '+':                          //CW +
        if (cwpt == 0){
          digitalWrite(cw_p, LOW);
          cwpt = 1;
        }
        else{
          digitalWrite(cw_p, HIGH);
          cwpt = 0;
        }
        break;
      case '-':                          //CCW +
        if (ccwpt == 0){
          digitalWrite(ccw_p, LOW);
          ccwpt = 1;
        }
        else{
          digitalWrite(ccw_p, HIGH);
          ccwpt = 0;
        }
        break;
      case ' ':                            //ZHOME
        digitalWrite(in4, LOW);
        delay(100);
        digitalWrite(in4, HIGH);
        Serial.write("zzzz");
        break;
      case '!':                           //STOP
        if(stop_state == 0){
          stop_state = 1;
          digitalWrite(in6, LOW);
          Serial.write("stop");
        }
        else {
          stop_state = 0;
          digitalWrite(in6, HIGH);
          Serial.write("ress");
        }
        break;
      case '#':                          //RV-JOG
        if(rvjog == 0){
          rvjog = 1;
          digitalWrite(in9, LOW);
          Serial.write("rvjg");
        }
        else {
          rvjog = 0;
          digitalWrite(in9, HIGH);
          Serial.write("strj");
        }
        break;
      case '$':                          //FW-JOG
        if(fwjog == 0){
          fwjog = 1;
          digitalWrite(in8, LOW);
          Serial.write("fwjg");
        }
        else {
          fwjog = 0;
          digitalWrite(in8, HIGH);
          Serial.write("stfj");
        }
        break;
      case '%':                          //ALM-RST
        digitalWrite(in7, LOW);
        delay(100);
        digitalWrite(in7, HIGH);
        Serial.write("arst");
        break;
      case '&':                          //PLS-RDY
        if(digitalRead(out2) == LOW){
          Serial.write("plsr");
        }
        else{
          Serial.write("plnr");
        }
        break;
      case '*':                          //MOVE
        if(digitalRead(out4) == LOW){
          Serial.write("mmmm");
        }
        else{
          Serial.write("fmmm");
        }
        break;
      case '=':                          //FREE
        if(free_state == 0){
          free_state = 1;
          digitalWrite(in5, LOW);
          Serial.write("free");
        }
        else {
          free_state = 0;
          digitalWrite(in5, HIGH);
          Serial.write("ntfr");
        }
        break;
      case '>':                           //READY
        if(digitalRead(out3) == LOW){
          Serial.write("rrdy");
        }
        else{
          Serial.write("nrdy");
        }
        break;
      case '?':                          //IN-POS
        if(digitalRead(out1) == LOW){
          Serial.write("inpo");
        }
        else{
          Serial.write("ninp");
        }
        break;
      case '@':                          //HOME-END
        if(digitalRead(out4) == LOW){
          Serial.write("wait");
          break;
        }
        else if(digitalRead(out0) == LOW){
          Serial.write("HOME");
          break;
        }
        else{
          Serial.write("errr");
          break;
        }
      case 'A':                         //ALM-B
        if(digitalRead(out5) == LOW){
          Serial.write("alrm");
        }
        else{
          Serial.write("cont");
        }
        break;
      default:
        //Serial.write("Command Not Found");
        Serial.write(serInput);
    }   
  }
}
