import threading
import queue
import socket
import time
import sys
import os
import logging
import kivy.resources
from datetime import date
import numpy as np
import pandas as pd
import serial as ser
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager
import serial.tools.list_ports
from kivy.properties import ListProperty
import matplotlib.pyplot as plt


class testArea:

    def __init__(self, ard=None, mk_ten=None, isaac=None, entris=None, testSpecs=None):
        self._ard = ard
        self._mk_ten = mk_ten
        self._isaac = isaac
        self._entris = entris
        self._testSpecs = testSpecs
        self.lock = threading.Lock()

    @property
    def ard(self):
        return self._ard

    @ard.setter
    def ard(self, ardComSel):
        if ardComSel == None:
            self._ard = None
        elif ardComSel[:3] != 'COM':
            print("str process error")
            self._ard = None
        else:
            try:
                print("setting ard")
                self._ard = self.initSer(ardComSel)
            except ser.serialutil.SerialException:
                self._ard = None
            if self.ard == None:
                print("Couldn't Init Serial")
            else:
                try:
                    self._ard.write(b'I')
                    self.resetALRM()
                except TimeoutError:
                    self._ard = None

    @property
    def mk_ten(self):
        return self._mk_ten

    @mk_ten.setter
    def mk_ten(self, mktenCOMsel):
        if mktenCOMsel == None:
            self._mk_ten = None
        elif mktenCOMsel[:3] != 'COM':
            self._mk_ten = None
        else:
            try:
                self._mk_ten = self.initSer(mktenCOMsel)
            except ser.serialutil.SerialException:
                self._mk_ten = None
            try:
                self.readMk10()
            except TimeoutError:
                self._mk_ten = None

    @property
    def isaac(self):
        return self._isaac

    @isaac.setter
    def isaac(self, isaacIPinput):
        if isaacIPinput == None:
            self._isaac = None
        elif len(isaacIPinput.split('.')) != 4:
            self._isaac = None
        else:
            try:
                self._isaac = self.initPressure(isaacIPinput)
            except TimeoutError:
                self._isaac = None
            if self.isaac == None:
                pass
            else:
                try:
                    type = self.isaac.send('RTT]'.encode())
                    ret = self.isaac.recv(5)
                except TimeoutError:
                    self._isaac = None

    @property
    def entris(self):
        return self._entris

    @entris.setter
    def entris(self, entrisCOMsel):
        if entrisCOMsel == None:
            self._entris = None
        elif entrisCOMsel[:3] != 'COM':
            self._entris = None
        else:
            try:
                self._entris = self.initEntris(entrisCOMsel)
            except ser.serialutil.SerialException:
                print("except ser")
                self._entris = None
            try:
                esc = chr(27).encode('ascii')
                cr = chr(ord('\r')).encode('ascii')
                lf = chr(ord('\n')).encode('ascii')
                prnt = 'P'.encode('ascii')
                tare_str = esc + prnt + cr + lf
                self.entris.write(tare_str)
            except TimeoutError:
                print("except TimeOut")
                self._entris = None

    @property
    def testSpecs(self):
        return self._testSpecs

    @testSpecs.setter
    def testSpecs(self, testSpecsInput):
        self.testSpecs = testSpecsInput

    read_entris_queue = queue.Queue()

    def initPressure(self, ip='192.168.1.2', port=23, buffSize=96):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((ip, port))
        s.settimeout(15)
        return s

    def initSer(self, inputPort):
        """Inits serial connection to kwarg string port"""
        cer = ser.Serial()
        cer.baudrate = 115200
        cer.port = inputPort
        cer.timeout = 10
        cer.open()
        return cer

    def initEntris(self, inputPort):
        """Inits serial connection to kward string port
            specialized settings for Entris Balance"""
        cer = ser.Serial()
        cer.baudrate = 19200
        cer.port = inputPort
        cer.timeout = 10
        cer.bytesize = ser.EIGHTBITS
        cer.parity = ser.PARITY_ODD
        cer.stopbits = ser.STOPBITS_ONE
        cer.xonxoff = True
        cer.rtscts = True
        cer.open()
        return cer

    def check_rdy(self):
        """Checks READY and PLS-RDY output signals and waits until ready, recursive
        IN - cer - pyserial.Serial Object
        OUT -> rdy - (0 if error), (1 if both ready)"""
        rdy = 0
        if self.ard.is_open:
            while self.check_move():
                time.sleep(0.5)
            self.ard.write(b'>')
            r = self.ard.read(4)
            if r == b'rrdy':
                self.ard.write(b'&')
                pr = self.ard.read(4)
                if pr == b'plsr':
                    rdy = 1
                elif pr == b'plnr':
                    rdy = self.check_rdy()
                else:
                    print("IO Error 2")
                    logging.info("Serial IO error, PULSE-RDY")
            elif r == b'nrdy':
                rdy = self.check_rdy()
            else:
                print("IO Error 1")
                logging.info("Serial IO error, READY")
        else:
            print("Serial Error, Not Open")
            logging.info("Check Ready Serial COM Error")
        return rdy

    def resetALRM(self):
        """Resets OrientalMotor Driver Alarm, 100ms delay
        IN - cer - pyserial Serial Object
        OUT - (0 if successful), (1 if error)"""
        self.ard.write(b'%')
        r = self.ard.read(4)
        if (r == b'arst'):
            return 0
        else:
            return 1

    def check_move(self):
        """Checks if MOVE signal and moving.
        OUT - (0 if not moving), (1 if moving), (2 if error)"""
        if self.ard.is_open:
            self.ard.write(b'*')
            r = self.ard.read(4)
            if r == b'mmmm':
                return 1
            elif r == b'fmmm':
                return 0
            else:
                print("move error")
                logging.info("Serial COM error - check_move - MOVE")
                return 2
        else:
            return 2

    def jog_fwd(self, pulses):
        """Conducts FWD JOG operation.  Not exact number of pulses.  Speed/pulse_per_distance not set yet MEXE02
        IN : cer - serial object (arudino),
             pulses - Number of Pulses to JOG FWD
        OUT -. (0 if successful), (1 if unsuccessful)"""
        if (pulses == 0):
            return 0
        travel_amt = 0.01  # mm
        op_spd = 6.64  # mm/s
        a = 10000  # m/s^2 accel and decel
        start_spd = 5  # mm/s
        op_spd_h = 50  # high speed maneuvering (how?) lol
        # dead distance (due to acceleration is 1 pulse)
        ret = 1
        trav = pulses * travel_amt - travel_amt  # mm to travel
        jog_pulse_time = trav / op_spd  # in second
        self.check_rdy()
        self.ard.write(b'$')
        if self.ard.read(4) == b'fwjg':
            time.sleep(jog_pulse_time)
            self.ard.write(b'$')
            if self.ard.read(4) == b'stfj':
                ret = 0
            else:
                self.ard.write(b'!')  # STOP toggle
                ret = 1
        else:
            print("Serial Com Error Jog Fwd")
            logging.info("Serial COM error - jogFwd")
        return ret

    def toggleFWDjog(self):
        """Toggles FWD Jog.
        IN:  cer - pyserial Serial object (arduino)
        OUT: (0 if stop jog), (1 if jog), (2 if error, toggles Stop)"""
        self.ard.write(b'$')
        r = self.ard.read(4)
        if (r == b'fwjg'):
            return 1
        elif (r == b'stfj'):
            return 0
        else:
            stop = self.toggleSTOP()
            if stop == 0:
                stop = self.toggleSTOP()
            return 2

    def jog_rev(self, pulses):
        """Conducts REVERSE JOG operation.  Not exact number of pulses.  Speed/pulse_per_distance not set yet MEXE02
            IN : cer - serial object (arudino),
                 pulses - Number of Pulses to JOG FWD
            OUT -. (0 if successful), (1 if unsuccessful)"""
        if (pulses == 0):
            return 0

        travel_amt = 0.01  # mm
        op_spd = 6.64  # mm/s
        ret = 1
        trav = pulses * travel_amt - travel_amt  # mm to travel
        jog_pulse_time = trav / op_spd  # in second
        self.check_rdy()

        if self.ard.is_open:
            self.ard.write(b'#')
            if self.ard.read(4) == b'rvjg':
                time.sleep(jog_pulse_time)
                self.ard.write(b'#')
                if self.ard.read(4) == b'strj':
                    ret = 0
                else:
                    stop = self.toggleSTOP()  # STOP toggle
                    ret = stop
            else:
                print("Serial Com Error Jog Rev")
        else:
            print("Serial Not Open Rev Jog")
            logging.info("Serial COM error revJog")
        return ret

    def toggleREVjog(self):
        """Toggles REV Jog.
        IN:  cer - pyserial Serial object (arduino)
        OUT: (0 if stop jog), (1 if jog), (2 if error, toggles Stop)"""
        self.ard.write(b'#')
        r = self.ard.read(4)
        if (r == b'rvjg'):
            return 1
        elif (r == b'strj'):
            return 0
        else:
            stop = self.toggleSTOP()
            if stop == 0:
                stop = self.toggleSTOP()
            return 2

    def zHOME(self):
        """Conducts ZHOME operation.  Speed not set/unsure, set with MEXE02
        IN - cer - serial object (arduino)
        OUT - (0 if successful), (1 if unsuccessful)"""
        # check_ready
        if self.check_rdy() == 1:
            self.ard.write(b' ')
            r = self.ard.read(4)
            if r == b'zzzz':
                return 0
            else:
                return 1
        else:
            return 1

    def end_HOME(self):
        """Checks for HOME-END output signal
        IN - cer - serial object (arduino)
        OUT - (0 if home), (1 if unsuccessful) """
        self.ard.write(b'*')
        r = self.ard.read(4)
        if r == b'mmmm':
            time.sleep(1)
            return self.end_HOME()
        elif r == b'fmmm':
            self.ard.write(b'@')
            r = self.ard.read(4)
            while (r == b'wait'):
                self.ard.write(b'@')
                r = self.ard.read(4)
            if r == b'HOME':
                return 0
            else:
                return 1
        else:
            return 1

    def toggleFree(self):
        """Frees the Motor and magnetic brake, allowing manual movement of the slide.  WARNING this will cause the slide to drop unless held.
        IN - cer - serial object (arduino)
        OUT - (O if free), (1 if locked), (2 if unsuccessful)"""
        self.ard.write(b'=')
        r = self.ard.read(4)
        if (r == b'free'):
            return 0
        elif (r == b'ntfr'):
            return 1
        else:
            return 2

    def toggleSTOP(self):
        """Emergency STOP signal for motor and actuator.  Toggle.
        IN - cer - serial object (arduio)
        OUT - (0 if return), (1 if STOP), (2 if unsuccessful)"""
        self.ard.write(b'!')
        r = self.ard.read(4)
        if (r == b'stop'):
            return 1
        elif (r == b'ress'):
            return 0
        else:
            return 2

    def checkALRM(self):
        """Checks alarm signal from OriMotor Driver
        IN - cer - pyserial Serial Object (arduino)
        OUT - (0 if no ALRM), (1 if ALRM), (2 if error)"""
        self.ard.write(b'A')
        r = self.ard.read(4)
        if (r == b'alrm'):
            return 1
        elif (r == b'cont'):
            return 0
        else:
            return 2

    def pulseOPcw(self, dist, speed):
        """Conducts clockwise pulse operation, based on distance and speed.
        IN:  cer - pyserial Serial Object (arduino)
             dist - Distance in (mm) to move slide (clockwise is DOWN)
             speed - Speed in mm/s to move
        OUT:    (0 if complete), (1 if error)"""

        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
        cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
        duty_time = cycle_time / 2.0  # (float) seconds time for each half of duty cycle
        maxPulseCycleTime = (115200 / 2.0) * (0.01)  # max pulse/sec

        if pulse_per_second > maxPulseCycleTime:
            print("Max Pulse Cycle Time, %s" % maxPulseCycleTime)
            return 1
        else:
            for i in range(pulses):
                self.ard.write(b'+')
                time.sleep(duty_time)
                self.ard.write(b'+')
                time.sleep(duty_time)
            return 0

    def pulseOPccw(self, dist, speed):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
        cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
        duty_time = cycle_time / 2.0  # (float) seconds time for each half of duty cycle
        maxPulseCycleTime = (115200 / 2.0) * (0.01)  # max pulse/sec

        if pulse_per_second > maxPulseCycleTime:
            return 1
        else:
            for i in range(pulses):
                self.ard.write(b'-')
                time.sleep(duty_time)
                self.ard.write(b'-')
                time.sleep(duty_time)
            return 0

    def pulseOPcwFast(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (clockwise is DOWN)
                 speed - 4.81mm/s
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        if pulses % 2 == 0:
            pulses = int(pulses / 2)
            pass
        else:
            pulses -= 1
            pulses = int(pulses / 2)
            self.ard.write(b'm')
        for i in range(pulses):
            self.ard.write(b'o')
            time.sleep(0.004)
        return 0

    def pulseOPccwFast(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        if pulses % 2 == 0:
            pulses = int(pulses / 2)
            pass
        else:
            pulses -= 1
            pulses = int(pulses / 2)
            self.ard.write(b'n')
        for i in range(pulses):
            self.ard.write(b'p')
            time.sleep(0.004)
        return 0

    def pulseOPcwFastEx(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (clockwise is DOWN)
                 speed - 4.81mm/s
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        for i in range(pulses):
            self.ard.write(b'm')
            time.sleep(0.00415)
        return 0

    def pulseOPccwFastEx(self, dist):
        """Conducts counter-clockwise pulse operation, based on distance and speed.
            IN:  cer - pyserial Serial Object (arduino)
                 dist - Distance in (mm) to move slide (counter=clockwise is UP)
                 speed - Speed in mm/s to move
            OUT:    (0 if complete), (1 if error)"""
        if (dist == 0):
            return 0
        pulses = int(dist / 0.01)  # number of pulses to move
        for i in range(pulses):
            self.ard.write(b'n')
            time.sleep(0.00415)
        return 0

    def readMk10(self):
        """"Reads current force reading on Mk10 Force gauge.
        IN:  cerMk10 - Mark10force gauge Serial Object
        OUT:  float curForce - current force  Newton"""
        qq = str.encode('?\r\n')
        self.mk_ten.write(qq)
        rdln = self.mk_ten.readline()
        split = rdln.split(b' ')
        try:
            curForce = float(split[0])
        except:
            curForce = 0.0
        return curForce

    def findPlunger(self):
        test = self.readMk10()
        rd = self.check_rdy()
        pulse = self.pulseOPccwFast(2)
        ret = self.check_rdy()
        jog = self.toggleFWDjog()
        while (test <= 0.2):
            if jog == 1:
                test = self.readMk10()
            else:
                jog = self.toggleFWDjog()
        jog = self.toggleFWDjog()
        rdy = self.check_rdy()
        pulse = self.pulseOPccwFast(1.5)
        test = self.readMk10()
        while (test <= 0.2):
            self.pulseOPcw(0.01, 0.75)
            test = self.readMk10()
        return 0

    def isaacChangeProgram(self, prog):
        """Uses Telnet commands to change IsaacHD program
        IN: sockIsaacHD - IsaacHD Socket Object
            prog - 2 digit unicode string of desired program.  Ex = '01'
        OUT: (0 if successful), (1 if error)"""
        change = 'SCP'
        changeProg = change + prog + ']'
        self.isaac.send(changeProg.encode())
        time.sleep(0.5)
        self.isaac.send('RCP]'.encode())
        ret = self.isaac.recv(5)
        cur_prog = repr(ret).split("\\")[0].split("'")[1]
        if cur_prog == prog:
            return 0
        else:
            return 1

    def isaacFlushSocket(self):
        """Flushes any pending data in socket buffer.  Prepares for test output.
        IN: sockIsaacHD - IsaacHD Socket Object
        OUT: None"""
        self.isaac.settimeout(3)
        try:
            flush = self.isaac.recv(1024)
        except socket.timeout:
            pass
        self.isaac.settimeout(15)

    def isaacStaticSealInit(self, staticSpec, staticTime):
        """Creates Program 01 on IsaacHD as Static Pressure Decay Test
        IN:   sockIsaacHD - IsaacHD Socket Object
              staticSpec - pressure Decay Spec - float
              staticTime - static test Timer FFF.f
        OUT: (0 if successful), (1 if error)"""
        lowSpecStr = 'SML' + str(staticSpec) + ']'
        highSpecStr = 'SMD' + str(staticSpec) + ']'
        specTime = 'ST6' + str(staticTime) + ']'
        ret = self.isaacChangeProgram('01')
        if ret == 0:
            time.sleep(0.5)
            settings = []
            settings.append('STT0]'.encode())
            settings.append('STP010.00]'.encode())
            settings.append('SPTP0.50]'.encode())
            settings.append('SPTM0.50]'.encode())
            settings.append('ST30.0]'.encode())
            settings.append('ST42.0]'.encode())
            settings.append('ST52.0]'.encode())
            settings.append(specTime.encode())
            settings.append('ST71.0]'.encode())
            settings.append('SVA1]'.encode())
            settings.append('SFA0]'.encode())
            settings.append(lowSpecStr.encode())
            settings.append(highSpecStr.encode())
            settings.append('SLD1]'.encode())
            settings.append('SER2]'.encode())
            for setting in settings:
                self.isaac.send(setting)
                time.sleep(0.1)
            return 1
        else:
            return 0

    def isaacSealOntheFlyinit(self, flySpec, testTime):
        """Creates Program 03 on IsaacHD as "On the Fly" Pressure Decay Test
            IN:   sockIsaacHD - IsaacHD Socket Object
            OUT: (0 if successful), (1 if error)"""
        lowSpecStr = 'SML' + str(flySpec) + ']'
        highSpecStr = 'SMD' + str(flySpec) + ']'
        testTimer = 'ST6' + str(testTime) + ']'
        ret = self.isaacChangeProgram('03')
        if ret == 0:
            time.sleep(0.5)
            settings = []
            settings.append('STT0]'.encode())  # PD
            settings.append('STP010.00]'.encode())  # Test Pressure
            settings.append('SPTP0.50]'.encode())  # Pressure +
            settings.append('SPTM0.50]'.encode())  # Pressure -
            settings.append('ST30.0]'.encode())  # Fast Fill Timer
            settings.append('ST42.0]'.encode())  # Fill Timer
            settings.append('ST52.0]'.encode())  # Settle Timer
            settings.append(testTimer.encode())  # Test Timer
            settings.append('ST71.0]'.encode())  # Vent Timer
            settings.append('SVA1]'.encode())  # Vent Auto Select
            settings.append('SFA0]'.encode())  # Fast Fill timer select
            settings.append(lowSpecStr.encode())
            settings.append(highSpecStr.encode())
            settings.append('SLD1]'.encode())  # End of Test Eval
            settings.append('SER2]'.encode())  # Output COM select
            for setting in settings:  # loop through settings, send cmd, wait required amt
                self.isaac.send(setting)
                time.sleep(0.1)
            return 1
        else:
            return 0

    def forceDispense(self, dispDist, dispSpeed):
        """Performs Dispense Operation, while recording Force.
        IN:  cerArd - Arduino Serial Object
             cerMk10 - Mark10 force gauge Serial Object
             dispDist - Volume to dispense (mm in distance) max 30
             dispSpeed - Speed to dispense (mm/s)
        OUT: Force vs. Dist NumpyArray during dispense"""
        pulses = int(dispDist / 0.01)  # number of pulses to move
        # init cerMk10
        itt = 0
        distArray = np.zeros(3000)
        force = np.zeros(3000)
        self.mk_ten.write(str.encode('Z\r\n'))
        rdy = self.check_rdy()
        if dispSpeed <= 1.5:
            # manual PulseOP
            pulse_per_second = dispSpeed / 0.01  # number of pulses per second to meet speed req
            cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
            duty_time = cycle_time / 2.0  # (float) seconds time for each half of duty cycle
            for i in range(pulses):
                force[i] = self.readMk10()
                distArray[i] = i * 0.01
                self.ard.write(b'+')
                time.sleep(duty_time)
                self.ard.write(b'+')
                time.sleep(duty_time)
                itt += 1
        elif dispSpeed == 2.41:
            for i in range(pulses):
                force[i] = self.readMk10()
                distArray[i] = i * 0.01
                self.ard.write(b'm')
                time.sleep(0.00415)
                itt += 1
        elif dispSpeed == 4.81:
            duty_time = 0.0015
            if (pulses % 2) == 0:
                speedPulse = pulses / 2
            else:
                pulses -= 1
                speedPulse = pulses / 2
                self.ard.write(b'm')
            for i in range(int(speedPulse)):
                self.ard.write(b'o')
                force[i] = self.readMk10()
                distArray[i] = i * 0.02
                # time.sleep(duty_time)
                itt += 1
        else:
            print("Invalid Dispense Speed")
            logging.info("Invalid Dispense Speed")
        return distArray[:itt], force[:itt]

    def speedFullStroke(self, dist):
        """Performs Full cycle from syringe position 0mm (bottom) at 2500uL/min (3.32 mm/s)
        IN:  cerArd - Arduino Serial Object
        OUT: (0 if successful), (1 if error)
              perfTime - Time to perform action"""
        time_0 = time.perf_counter()
        rdy = self.check_rdy()
        ret = self.pulseOPccwFast(dist)
        rdy = self.check_rdy()
        ret = self.pulseOPcwFast(dist)
        time_2 = time.perf_counter()
        total_time = time_2 - time_0
        if ret == 1:
            return 1, total_time
        else:
            return 0, total_time

    def speedAspirateTest(self, strokeDist, aspSpeed):
        """Performs Speed to Aspirate Operation. 25mm travel
        IN:  cerArd - Arduino Serial Object
             cerMk10 - Mark10 force gauge Serial Object
        OUT: Force and Dist NumpyArray during aspirate"""
        pulses = strokeDist / 0.01
        # init cerMk10
        itt = 0
        distArray = np.zeros(3000)
        force = np.zeros(3000)

        self.mk_ten.write(str.encode('Z\r\n'))
        travel_amt = 0.01  # mm
        curPos = 0
        t_zero = time.perf_counter()
        if self.ard.is_open:
            if aspSpeed <= 1.25:
                pulseSpeed = aspSpeed * 100  # pulses/sec
                duty_time = (1 / pulseSpeed) / 2
                for i in range(pulses):
                    force[i] = self.readMk10()
                    distArray[i] = curPos
                    curPos += 0.01
                    self.ard.write(b'-')
                    time.sleep(duty_time)
                    self.ard.write(b'-')
                    time.sleep(duty_time)
                    itt += 1
            elif aspSpeed == 2.41:
                duty_time = 0.00415
                for i in range(pulses):
                    force[i] = self.readMk10()
                    distArray[i] = curPos
                    curPos += 0.01
                    self.ard.write(b'n')
                    time.sleep(duty_time)
                    itt += 1
            elif aspSpeed == 4.81:
                duty_time = 0.0015
                if (pulses % 2) == 0:
                    speedPulse = pulses / 2
                else:
                    pulses -= 1
                    speedPulse = pulses / 2
                    self.ard.write(b'n')
                    curPos += 0.01
                for i in range(int(speedPulse)):
                    force[i] = self.readMk10()
                    distArray[i] = curPos
                    curPos += 0.02
                    self.ard.write(b'p')
                    # time.sleep(duty_time)
                    itt += 1
            else:
                print("Invalid Speed")
                logging.info("Invalid Aspiration Speed")
        else:
            print("Serial COM Error Rev Jog")
            logging.info("Serial COM error rev Jog")
            return 0, 0
        return distArray[:itt], force[:itt]

    def sealTest_static(self):
        """Performs a Static PD test.  Returns results
        IN:  sockIsaacHD - IsaackHD Socket Object
        OUT: str result - Classification of Result (Ex. Leak, Pass, Gross Leak, High Pressure)
             float leak - leak amount"""
        self.isaacChangeProgram('01')
        self.isaacFlushSocket()
        self.isaac.send('SRP]'.encode())
        rawOutputStr = self.isaac.recv(100).split(b'\t')
        #print(rawOutputStr)
        result = str(rawOutputStr[5], 'utf-8')
        leak = float(rawOutputStr[6])
        return result, leak

    def sealTest_OnTheFly(self, cycles, testT, dist):
        """"Performs a dynamic PD test. From syringe position 25mm.  Returns results.
        IN:  cerArd - Arduino Serial Object
             sockIsaacHD - IsaacHD Pressure Tester Socket Object
             cycles - number of aspiration/dispense cycles during test
             testT - time to test pressure
             speed - Speed to cycle (mm/s)
            (default) distance is 25mm
        OUT: str result - Classification of Result (Ex. Leak, Pass, Gross Leak, High Pressure)
             float leak - leak amount """
        self.isaacChangeProgram('03')
        testTime = testT
        ret = self.pulseOPcwFast(dist)
        if (ret == 0) and (self.check_rdy()):
            total_time = 0
            time_0 = time.perf_counter()
            self.isaac.send('SRP]'.encode())
            time.sleep(4)
            self.isaacFlushSocket()
            for cyc in range(cycles):
                ret, time_cyc = self.speedFullStroke(dist)
                if ret == 0:
                    total_time += time_cyc
                else:
                    break
            if (total_time < testTime):
                time.sleep(((testTime - 7) - total_time))
                try:
                    rawOutputStr = self.isaac.recv(100).split(b'\t')
                except socket.timeout:
                    rawOutputStr = [None, None, None, None, None, b'Fail', 2.6]
            else:
                print("cycleTimeError")
                return 0, 0
        else:
            return 0
        result = str(rawOutputStr[5], 'utf-8')
        leak = float(rawOutputStr[6])
        return result, leak

    def dataOut_makedf(self, testNum, forceDispRes, leakDict, forceAspRes):
        """Creates output dataframes.
        IN - testNum - number of test within cycle
             forceDispRes - npArray that is output of CycleTest
             staticResultArray - array of Result strings from static seal tests at each park_pos
             otfSealResult - string result of otf seal test
             forceAspRes - npArray that is output of CycleTest
        OUT: dfs that hold data for this test.  combine to workbook before next cycle"""
        dispDF = pd.DataFrame(
            {('Displacement_' + str(testNum)): forceDispRes[0], ('Force_' + str(testNum)): forceDispRes[1]})
        sealTestDF = pd.DataFrame(leakDict, index=[0])
        aspdF = pd.DataFrame(
            {('Displacement_' + str(testNum)): forceAspRes[0], ('Force_' + str(testNum)): forceAspRes[1]})
        return dispDF, sealTestDF, aspdF

    def combine_DataFrames(self, combDispdf, combSealTestdf, combAspdf, newDispdf, newSealdf, newAspdf):
        """Combines dataframes from each Test for each cycle
        IN - combDispdf - combined Dataframe of Dispense Force Data
             combSealTestdf - combined SealTest dataframe
             combAspdf - combined Aspiration  Force Dataframe
             newDispdf - new data to add
             newSealdf - new data to add
             newAspdf - new data to add
        OUT -combDispdf - combined Dataframe of Dispense Force Data
             combSealTestdf - combined SealTest dataframe
             combAspdf - combined Aspiration  Force Dataframe"""
        combineDispDf = combDispdf.join(newDispdf, how='outer')
        combineAspDf = combAspdf.join(newAspdf, how='outer')
        combineSealDf = pd.concat([combSealTestdf, newSealdf])
        return combineDispDf, combineSealDf, combineAspDf

    def makeExcelOut(self, partID, cycNum, combDispdf, combSealTestdf, combAspdf):
        """Creates output Excel Workbook from combined Dataframes.
        IN - partID - Test Pump ID String
             cycNum - Cycle Number
             park_pos - LIST of park positions for static seal tests
             combDispdf - combined Dataframe of Dispense Force Data
             combSealTestdf - combined SealTest dataframe
             combAspdf - combined Aspiration  Force Dataframe
        OUT - (0 if successful), (1 if unsuccessful)"""
        today = str(date.today()).replace("-", "")
        writer = pd.ExcelWriter(today + "_" + str(partID) + "_TestResults_" + str(cycNum) + ".xlsx",
                                engine='xlsxwriter')
        # calc Dispense Result Analysis
        fDispName = 'ForceDispenseResults'
        combDispdf.to_excel(writer, sheet_name=fDispName)
        displ_cols = [col for col in combDispdf.columns if 'Displacement_' in col]
        force_cols = [col for col in combDispdf.columns if 'Force_' in col]
        displ_cols.sort()
        force_cols.sort()
        returnDict = {}
        for f, d in zip(force_cols, displ_cols):
            idx = combDispdf[[f]].idxmax(axis=0)
            v = combDispdf.iloc[idx][d]
            val = combDispdf[[f]].max(axis=0)
            avg = combDispdf[[f]].mean(axis=0)
            outL = [round(avg[0], 2), val[0], v.values[0]]
            returnDict[f + 'Results'] = outL
        returnDict['Results_Index'] = ['Average Force (N)', 'Maximum Force (N)', 'Displacement @ Max Force (mm)']
        res = pd.DataFrame(returnDict)
        res.to_excel(writer, sheet_name=fDispName, startcol=11)
        # sealTest Results are fine as is
        sealName = 'SealTestResults'
        combSealTestdf.to_excel(writer, sheet_name=sealName)
        # Calc Aspirate Result Analysis
        aspName = 'ForceAspirateResults'
        combAspdf.to_excel(writer, sheet_name=aspName)
        aspAnalysis = self.aspirAnalysis(combAspdf)
        aspAnalysis.to_excel(writer, sheet_name=aspName, startcol=11)
        writer.save()
        return 0

    def aspirAnalysis(self, disp):
        displ_cols = [col for col in disp.columns if 'Displacement_' in col]
        force_cols = [col for col in disp.columns if 'Force_' in col]
        force_cols.sort()
        returnDict = {}
        tmp = pd.DataFrame()
        for i in range(len(displ_cols)):
            tmp_str = 'Low_F_' + str(i)
            tmp[tmp_str] = disp[force_cols[i]] <= 0.2
            tmp_lst = tmp[tmp_str].tolist()
            count = 0
            state = 0
            itt = 0
            for boo in tmp_lst:
                if (boo == True) and (state == 0):
                    state = 1
                    itt = 1
                elif (boo == True) and (state == 1):
                    itt += 1
                    if itt == 5:
                        count += 1
                else:
                    state = 0
                    itt = 0
            returnDict['Test#' + str(i + 1) + '_Plunger Separations (0.1mm)'] = count
        ana = pd.DataFrame(returnDict, index=[0])
        return ana

    def cycleTest(self, in_queue, out_queue, itter, stroke_dist, fDispSpeed, staticSealPos, numCycles, testT, aspSpeed):
        """Performs complete Cycle Test.
        IN:  cerArd - Arduino Serial Object
             sockIsaacHD - IsaacHD Pressure Tester Socket Object
             cerMk10 - Mark10 Force Sensor Serial Object
             fDispSpeed - speed for forceDispense test
             List staticSealPos - list of position to test Static Seal
             numCycles - number of aspiration/dispense cycles during OnTheFlySeal test
        OUT: forceDispRes - dist, force npArray during forceDispense
             staticResultArray - npArray of static seal results
             staticLeakArray - npArray of static seal leaks
             otfSealResult - On the Fly seal results
             otfSealLeak - On the Fly seal Leak
             forceAspRes - dist, force npArray during speedAspirate
             """
        staticLeakArray = np.zeros((len(staticSealPos)), dtype=float)
        pos = stroke_dist
        print("enter test")
        logging.info("Test Cycle: %s, Start Individual Test Cycle %s" % (str(itter[0]), str(itter[1])))
        ret = self.checkPauseQue(in_queue)
        ret = self.check_rdy()
        out_queue.put(["Dispense Force Test", itter[0], itter[1], 1], block=False)  # 1== force dispense
        print("Force Dispense")
        logging.info("Test Cycle: %s, Individual Test: %s, Force Dispense" % (str(itter[0]), str(itter[1])))
        distDisp, forceDisp = self.forceDispense(pos, fDispSpeed)
        pos = 0
        pulsePos = 0
        # check static pos for userErrors
        for posList in staticSealPos:
            if posList < 0:
                print("invalid static seal pos")
                logging.info("Invalid Static Seal Positions (position<0")
                return forceDisp, 1, 1, 1
            elif posList > stroke_dist:
                print("invalid static seal pos")
                logging.info("Invaluid Static seal pos > max_stroke_dist")
                return forceDisp, 1, 1, 1
        itt = 0
        ret = self.checkPauseQue(in_queue)
        out_queue.put(["Static Seal Testing", itter[0], itter[1], 2], block=False)  # 2 == static Sealing
        print("Static Seal")
        logging.info("Test Cycle: %s, Individual Test: %s, Static Seal Start" % (str(itter[0]), str(itter[1])))
        staticSealPos.sort()
        ret = self.check_rdy()
        leakDict = {}
        for posL in staticSealPos:
            ret = self.checkPauseQue(in_queue)
            logging.info(
                "Test Cycle: %s, Individual Test: %s, Static Seal Pos %s" % (str(itter[0]), str(itter[1]), str(itt)))
            movePulse = posL - pos
            ret = self.pulseOPccwFast(movePulse)
            pos += movePulse
            result, leak = self.sealTest_static()
            leakDict["Static Seal Result @ " + str(posL) + "mm"] = result
            leakDict["Static Seal Leak Amount @" + str(posL) + "mm (psig)"] = leak
            itt += 1
        # check pulsepos
        if pos < stroke_dist:
            diff = stroke_dist - pos
            ret = self.pulseOPccwFast(diff)
            pos = stroke_dist
        elif pos > stroke_dist:
            diff = pos - stroke_dist
            ret = self.pulseOPcwFast(diff)
            pos = stroke_dist
        print("exit Static Seal")
        ret = self.checkPauseQue(in_queue)
        out_queue.put(["On The Fly Seal Testing", itter[0], itter[1], 3], block=False)  # 3== OTF test
        print("otf Seal")
        logging.info("Test Cycle: %s, Individual Test: %s, On The Fly Test Start" % (str(itter[0]), str(itter[1])))
        otfSealResult, otfSealLeak = self.sealTest_OnTheFly(numCycles, testT, stroke_dist)
        leakDict["On The Fly Test Result"] = otfSealResult
        print("exit otf")
        logging.info("Test Cycle: %s, Individual Test: %s, On The Fly Test Finish" % (str(itter[0]), str(itter[1])))
        time.sleep(1)
        ret = self.checkPauseQue(in_queue)
        ret = self.check_rdy()
        out_queue.put(["Speed Aspiration Test", itter[0], itter[1], 4], block=False)  # 4 == Speed Aspirate
        print("speed asp")
        logging.info(
            logging.info("Test Cycle: %s, Individual Test: %s, Speed Aspiration Test" % (str(itter[0]), str(itter[1]))))
        distAsp, forceAsp = self.speedAspirateTest(stroke_dist, aspSpeed)
        forceDispRes = np.vstack((distDisp, forceDisp))
        forceAspRes = np.vstack((distAsp, forceAsp))
        print("exit test")
        logging.info("Test Cycle: %s, End Individual Test: %s" % (str(itter[0]), str(itter[1])))
        return forceDispRes, leakDict, forceAspRes

    def fullTest(self, testSpecs, itt, in_queue, out_queue):
        testParkPos = testSpecs.parkPos[0]
        test_itt = 1
        combineDispDf = None
        combineSealDf = None
        combineAspDf = None
        for testSpec in testSpecs.test:
            self.mk_ten.write(str.encode('Z\r\n'))
            self.checkPauseQue(in_queue)
            self.check_rdy()
            out_queue.put(["Finding Plunger", itt, test_itt, 0], block=False)
            print("Find Plunger")
            logging.info("Finding Plunger")
            found = self.findPlunger()
            forceDispRes, leakDict, forceAspRes = self.cycleTest(in_queue, out_queue, [itt, test_itt],
                                                                 testSpecs.strokeDist, testSpec, testParkPos,
                                                                 testSpecs.otfNumCycles, testSpecs.otftestTime,
                                                                 testSpecs.aspSpeed)
            ret = self.checkPauseQue(in_queue)
            out_queue.put(["Making Data Output", itt, test_itt, 5], block=False)  # 5 == make data out
            logging.info("Making Data Output")
            leakDict["Test #"] = str(test_itt)
            if combineDispDf is None:
                t1_dispdf, t1_sealTest, t1_aspdf = self.dataOut_makedf(test_itt, forceDispRes, leakDict, forceAspRes)
                combineDispDf, combineSealDf, combineAspDf = self.dataOut_makedf(test_itt, forceDispRes, leakDict,
                                                                                 forceAspRes)
            else:
                t1_dispdf, t1_sealTest, t1_aspdf = self.dataOut_makedf(test_itt, forceDispRes, leakDict, forceAspRes)
                combineDispDf, combineSealDf, combineAspDf = self.combine_DataFrames(t1_dispdf, t1_sealTest, t1_aspdf,
                                                                                     combineDispDf, combineSealDf,
                                                                                     combineAspDf)
            test_itt += 1
        logging.info("Saving Excel Output")
        ret = self.makeExcelOut(testSpecs.partID, itt, combineDispDf, combineSealDf, combineAspDf)

    def speedFullStrokeParkTest(self, strokeDist, numCycles, speed, parkTlen, parkPos, in_queue, out_queue, out_test):
        """Performs full Stroke with park at parkPos for parkTlen.  Starts at 25mm.
        IN:     cerArd -    Arduino serial object
                numCycles - number of repeats full strokes with park up+down = 1
                speed -     rate (mm/s) to cycle
                parkTlen -  Length of Time to stop at each parkPos (seconds)
                parkPos -   [List] of positions to park at during cycle
        OUT:    ret - (0 if complete), (1 if error)"""
        for posList in parkPos:
            if posList < 0:
                print("invalid static seal pos")
                return 1
            elif posList > 25:
                print("invalid static seal pos")
                return 1
        parkPos = [strokeDist if pos > strokeDist else pos for pos in parkPos]
        pos = strokeDist
        if speed <= 1.5:
            for cyc in range(numCycles):
                self.checkPauseQue(in_queue)
                out_queue.put(["Full Stroke Cycle with Park", out_test, 90, cyc], block=False)
                logging.info("Full Stroke Cycle with Park #%s" % str(cyc))
                parkPos.sort(reverse=True)
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = pos - park
                    if move == 0:
                        continue
                    self.pulseOPcw(move, speed)
                    time.sleep(parkTlen)
                    pos = park
                if pos != 0:
                    move = pos - 0
                    self.pulseOPcw(move, speed)
                    time.sleep(parkTlen)
                    pos = 0
                parkPos.sort()
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = park - pos
                    if move == 0:
                        continue
                    self.pulseOPccw(move, speed)
                    time.sleep(parkTlen)
                    pos = park
                if pos != strokeDist:
                    move = strokeDist - pos
                    self.pulseOPccw(move, speed)
                    time.sleep(parkTlen)
                    pos = strokeDist
        elif speed == 2.41:
            for cyc in range(numCycles):
                self.checkPauseQue(in_queue)
                out_queue.put(["Full Stroke Cycle with Park", out_test, 90, cyc], block=False)
                logging.info("Full Stroke Cycle with Park #%s" % str(cyc))
                parkPos.sort(reverse=True)
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = pos - park
                    if move == 0:
                        continue
                    self.pulseOPcwFastEx(move)
                    time.sleep(parkTlen)
                    pos = park
                if pos != 0:
                    move = pos - 0
                    self.pulseOPcwFastEx(move)
                    time.sleep(parkTlen)
                    pos = 0
                parkPos.sort()
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = park - pos
                    if move == 0:
                        continue
                    self.pulseOPccwFastEx(move)
                    time.sleep(parkTlen)
                    pos = park
                if pos != strokeDist:
                    move = strokeDist - pos
                    self.pulseOPccwFast(move)
                    time.sleep(parkTlen)
                    pos = strokeDist
        elif speed == 4.81:
            for cyc in range(numCycles):
                self.checkPauseQue(in_queue)
                out_queue.put(["Full Stroke Cycle with Park", out_test, 90, cyc], block=False)
                logging.info("Full Stroke Cycle with Park #%s" % str(cyc))
                parkPos.sort(reverse=True)
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = pos - park
                    if move == 0:
                        continue
                    self.pulseOPcwFast(move)
                    time.sleep(parkTlen)
                    pos = park
                if pos != 0:
                    move = pos - 0
                    self.pulseOPcwFast(move)
                    time.sleep(parkTlen)
                    pos = 0
                parkPos.sort()
                for park in parkPos:
                    self.checkPauseQue(in_queue)
                    move = park - pos
                    if move == 0:
                        continue
                    self.pulseOPccwFast(move)
                    time.sleep(parkTlen)
                    pos = park
                if pos != strokeDist:
                    move = strokeDist - pos
                    self.pulseOPccwFast(move)
                    time.sleep(parkTlen)
                    pos = strokeDist
        else:
            print("Invalid Dispense Speed")
            logging.info("Invalid Recip Speed")
            return 1
        self.check_rdy()
        return 0

    def run_Spec(self, testSpecs, in_queue, out_queue):
        out_queue.put(["Setting Pressure Test Specs", None, None, None],
                      block=False)  # status string, outertest#, inner test#, testStatusComplete
        logging.info("Setting Pressure Test Specs")
        ret = self.isaacStaticSealInit(testSpecs.staticSealSpec, testSpecs.staticTime)
        ret = self.isaacSealOntheFlyinit(testSpecs.otfSealSpec, testSpecs.otftestTime)
        self.mk_ten.write(str.encode('Z\r\n'))
        # home = self.zHOME()
        for i in range(testSpecs.testRepeat):
            self.checkPauseQue(in_queue)
            out_queue.put(["Beginning Testing", i + 1, 0, 0], block=False)
            logging.info("Testing Begin")
            self.fullTest(testSpecs, i + 1, in_queue, out_queue)
            self.checkPauseQue(in_queue)
            logging.info("Full Stroke With Park Test Begin")
            self.speedFullStrokeParkTest(testSpecs.strokeDist, testSpecs.recipCycles, testSpecs.recipSpeed,
                                         testSpecs.parkTlen, testSpecs.speedPos, in_queue, out_queue, out_test=i + 1)
            logging.info("Full Stroke with Park Test End")
        out_queue.put(["End Test", 0, 100, 0], block=False)
        ret = self.check_rdy()
        home = self.zHOME()

    def checkPauseQue(self, in_queue):
        try:
            pause = in_queue.get(block=False)
            if pause == True:
                self.lock.acquire()
                print("Paused")
                while True:
                    try:
                        pause = in_queue.get(block=False)
                        if pause == False:
                            self.lock.release()
                            print("Resumed")
                            return 0
                    except queue.Empty:
                        continue
            else:
                print("Exiting")
                sys.exit()
        except queue.Empty:
            return 0

    def readEntris(self):

        esc = chr(27).encode('ascii')
        cr = chr(ord('\r')).encode('ascii')
        lf = chr(ord('\n')).encode('ascii')
        prnt = 'P'.encode('ascii')

        tare_str = esc + prnt + cr + lf
        self.entris.write(tare_str)
        ret = self.entris.read(16)
        r = ret.decode("utf-8")
        self.read_entris_queue.put(r, block=False)

    def readEntrisThread(self):
        entrthread = threading.Thread(target=self.readEntris)
        entrthread.daemon = True
        entrthread.start()

    def retr_entr_out(self):
        r = self.read_entris_queue.get(block=False)
        p = self.parseEntris(r)
        return p

    def tareEntris(self):
        esc = chr(27).encode('ascii')
        cr = chr(ord('\r')).encode('ascii')
        lf = chr(ord('\n')).encode('ascii')
        tare = chr(ord('T')).encode('ascii')

        tare_str = esc + tare + cr + lf
        self.entris.write(tare_str)
        return 0

    def parseEntris(self, s):
        """Parses Entris Output
        IN:  split- Entris return string, conv from bytes to str and split
        OUT: list - [pm, w, unit]
                    pm = + or -
                    w = weight
                    unit = unit"""
        split = s.split(' ')
        pm = None
        sp = None
        w = None
        unit = None
        ret = None
        for sp in split:
            if sp == '+' or s == '-':
                pm = sp
            elif sp == '':
                sp = sp
            elif sp == 'g':
                unit = sp
            elif sp == '\r\n':
                ret = sp
            else:
                w = sp
        return w

    def dispense_wet_test(self, distance, speed):
        """Conducts dispensing wet test, measuring dispense weight.  tares Before test.
        Returns time vs volume change np arrays"""
        pulses = int(distance / 0.01)  # number of pulses to move
        theoretical_proc_time = distance / speed
        flow_rate_time_arr = np.zeros(pulses + 200)
        itt = 1
        ret = self.tareEntris()
        rdy = self.check_rdy()
        time.sleep(5)
        r = self.clear_entr_queue()
        if speed <= 1.5:
            # manual PulseOP
            pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
            cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
            duty_time = (cycle_time / 2.0)*0.9 # (float) seconds time for each half of duty cycle
            flow_rate_time_arr[0] = 0.0
            self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            # flow_rate_vol_arr[0] = (ret[0], float(ret[1]), ret[2])
            for i in range(pulses):
                self.ard.write(b'+')
                time.sleep(duty_time)
                self.ard.write(b'+')
                if time.perf_counter() - s_zero >= 0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                else:
                    time.sleep(duty_time)
        elif speed == 2.41:
            flow_rate_time_arr[0] = 0.0
            ret = self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            for i in range(pulses):
                self.ard.write(b'm')
                if time.perf_counter() - s_zero >=0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                else:
                    time.sleep(0.0039)
        elif speed == 4.81:
            duty_time = 0.0015
            flow_rate_time_arr[0] = 0.0
            ret = self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            if (pulses % 2) == 0:
                speedPulse = pulses / 2
            else:
                pulses -= 1
                speedPulse = pulses / 2
                self.ard.write(b'm')
            for i in range(int(speedPulse)):
                self.ard.write(b'o')
                if time.perf_counter() - s_zero >= 0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                else:
                    time.sleep(0.00385)
        else:
            print("Invalid Dispense Speed")
            logging.info("Invalid Dispense Speed")
            return 0
        print(time.perf_counter()-t_zero)
        for i in range(15):
            ret = self.readEntrisThread()
            t = time.perf_counter()
            flow_rate_time_arr[itt] = t - t_zero
            time.sleep(0.2)
            itt += 1
        vols = self.clear_entr_queue()
        return flow_rate_time_arr[:itt], vols

    def aspirate_wet_test(self, distance, speed):
        """Conducts dispensing wet test, measuring dispense weight.  tares Before test.
        Returns time vs volume change np arrays"""
        pulses = int(distance / 0.01)  # number of pulses to move
        theoretical_proc_time = distance / speed
        seconds = theoretical_proc_time * 60
        samples = int((seconds * 5) + 15)
        flow_rate_time_arr = np.zeros(samples)
        itt = 1
        self.tareEntris()
        rdy = self.check_rdy()
        time.sleep(5)
        ret = self.clear_entr_queue()
        if speed <= 1.5:
            # manual PulseOP
            pulse_per_second = speed / 0.01  # number of pulses per second to meet speed req
            cycle_time = 1.0 / pulse_per_second  # (float) seconds time for each duty cycle
            duty_time = (cycle_time / 2.0)*0.9  # (float) seconds time for each half of duty cycle
            flow_rate_time_arr[0] = 0.0
            self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            for i in range(pulses):
                self.ard.write(b'-')
                time.sleep(duty_time)
                self.ard.write(b'-')
                if time.perf_counter() - s_zero >= 0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                    time.sleep(duty_time)
                else:
                    time.sleep(duty_time)
        elif speed == 2.41:
            flow_rate_time_arr[0] = 0.0
            ret = self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            for i in range(pulses):
                self.ard.write(b'n')
                if time.perf_counter() - s_zero >= 0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                else:
                    time.sleep(0.0039)
        elif speed == 4.81:
            duty_time = 0.0015
            ret = self.readEntrisThread()
            t_zero = time.perf_counter()
            s_zero = time.perf_counter()
            if (pulses % 2) == 0:
                speedPulse = pulses / 2
            else:
                pulses -= 1
                speedPulse = pulses / 2
                self.ard.write(b'n')
            for i in range(int(speedPulse)):
                self.ard.write(b'p')
                if time.perf_counter() - s_zero >= 0.2:
                    ret = self.readEntrisThread()
                    flow_rate_time_arr[itt] = time.perf_counter() - t_zero
                    s_zero = time.perf_counter()
                    itt += 1
                else:
                    time.sleep(0.00385)
        else:
            print("Invalid Dispense Speed")
            logging.info("Invalid Dispense Speed")
            return 0
        #print(time.perf_counter() - t_zero)
        for i in range(10):
            ret = self.readEntrisThread()
            t = time.perf_counter()
            flow_rate_time_arr[itt] = t - t_zero
            time.sleep(0.2)
            itt+=1
        vols = self.clear_entr_queue()
        return flow_rate_time_arr[:itt], vols

    def clear_entr_queue(self):
        vols = np.zeros((2200))
        i = 0
        itt = 0
        while i == 0:
            try:
                r = self.read_entris_queue.get(block=False)
                w = float(self.parseEntris(r))
                if itt == 0:
                    vols[itt] = 0.00
                    itt+=1
                    continue
                elif w == 0.00:
                    continue
                else:
                    vols[itt] = w
                    itt += 1
            except queue.Empty:
                i = 1
        return vols[:itt]

    def volume_to_distance(self, volume):
        """Converts Volume for asp/disp to distance for motor to travel
        - may need to update barrel diameter"""
        barrel_diam = 4.7  # mm
        csa = ((barrel_diam / 2) ** 2) * np.pi
        dist = volume / csa
        return dist

    def flowrate_to_speed(self, flow_rate):
        """Converts flow_rate to Motor travel speed based on barrel diameter
        - may need to update barrel diameter
        - may need to pigeonhole speeds"""
        barrel_diam = 4.7  # mm
        csa = ((barrel_diam / 2) ** 2) * np.pi
        speed = flow_rate / csa
        return speed

    def calcHyst(self, disp_vol, asp_vol):
        pass

    def calcflow_rate(self, time_arr, vol_arr):
        """Calculates flow rate from time and volume arrays"""
        # start by checking pm
        # not sure if this is needed???
        # check unit
        # not sure if this is needed??
        # create delta_t and delta_v arrays
        vlen = len(vol_arr)
        time_arr = time_arr[:vlen]
        delta_t = np.diff(time_arr)
        delta_v = np.diff(vol_arr)
        i = -1
        d = delta_v[i]
        while (d < 0.0005):
            i -= 1
            d = delta_v[i]
        delta_v = delta_v[:i]
        delta_t = delta_t[:i]
        i = 0
        d = delta_v[i]
        while (d < 0.001):
            i += 1
            d = delta_v[i]
        delta_v = delta_v[i:]
        delta_t = delta_t[i:]
        # calc flow rate array at each data point
        flow_rate = np.true_divide(delta_v[1:], delta_t[1:])
        # average flow rate calc
        avg_flowrate = np.mean(flow_rate)
        return vol_arr[-1], avg_flowrate, flow_rate

    def wetTest_abc(self, in_queue, out_queue, testSpecs, itt):
        """Performs wet tests ABC (wet aspiration and dispense) aspirate flow rate varies
        Returns combined pd.DataFrame of results"""
        logging.info("Wet Test ABC, Test# %s"%str(itt))
        out_queue.put(["Wet Test ABC, Test# %s"%str(itt)],block = False)
        asp_spds = testSpecs.wetTest_aspSpds
        abc_res = {}
        combine_df = None
        wet_disp_spd = testSpecs.wetTest_dispSpd
        i=0
        abc = ['Test A', 'Test B', 'Test C']
        for spd in asp_spds:
            print("Wet Test ABC, %s"%spd)
            self.checkPauseQue(in_queue)
            out_queue.put(["Wet Test Aspirate "+abc[i]], block=False)
            r = self.check_rdy()
            asp_time, asp_vols = self.aspirate_wet_test(20, spd)
            time.sleep(3)
            self.checkPauseQue(in_queue)
            out_queue.put(["Wet Test Dispense "+abc[i]], block=False)
            r = self.check_rdy()
            disp_time, disp_vols = self.dispense_wet_test(20, wet_disp_spd)
            time.sleep(3)
            print("Calc FR")
            fvol, avg_fr, frA = self.calcflow_rate(asp_time, asp_vols)
            abc_res['Aspirate Final Volume'] = fvol
            abc_res['Aspirate Avg Flow Rate'] = avg_fr
            fvol, avg_fr, frA = self.calcflow_rate(disp_time, disp_vols)
            abc_res['Dispense Final Volume'] = fvol
            abc_res['Dispense Avg Flow Rate'] = avg_fr
            abc_res['Aspiration Test Speed'] = spd
            abc_df = pd.DataFrame(abc_res, index=[0])
            if combine_df is None:
                combine_df = abc_df
            else:
                combine_df = pd.concat([combine_df, abc_df])
            i+=1
        return combine_df

    def wetTest_d(self, in_queue, out_queue, itt):
        """Performs Wet Test D.  Force aspirate and force dispense at 4.81mm/s
        Returns combined result pd.DataFrame"""
        logging.info("Wet Test D, Test# %s"%str(itt))
        out_queue.put(["Wet Test D, Test# %s"%str(itt)],block = False)
        logging.info("Speed Aspirate 0->20, 4.81 mm/s")
        distAsp, forceAsp = self.speedAspirateTest(20, 4.81)
        time.sleep(3)
        self.checkPauseQue(in_queue)
        print("Force Dispense")
        logging.info("Wet Test Force Dispense @ 4.81 mm/s")
        distDisp, forceDisp = self.forceDispense(20, 4.81)
        time.sleep(3)
        self.checkPauseQue(in_queue)
        ret = self.check_rdy()
        print("Data Cleanup")
        logging.info("Data Cleanup")
        forceDispRes = np.vstack((distDisp, forceDisp))
        forceAspRes = np.vstack((distAsp, forceAsp))
        dispDF = pd.DataFrame({'Dispense_Displacement': forceDispRes[0], 'Dispense_Force': forceDispRes[1]})
        aspDF = pd.DataFrame({'Aspirate_Displacement': forceAspRes[0], 'Aspirate_Force': forceAspRes[1]})
        testD_comb_res = dispDF.join(aspDF, how='outer')
        return testD_comb_res

    def wetTest_dfOut(self, abc_comb_df, d_df, itt, testSpecs):
        """Creates output Excel File from ABCD tests with analysis"""
        today = str(date.today()).replace("-", "")
        pid = testSpecs.partID
        writer = pd.ExcelWriter(today + "_" + str(pid) + "_TestResults_" + str(itt) + ".xlsx",
                                engine='xlsxwriter')
        abc_sheetname = 'Test_ABC_Results'
        abc_comb_df.to_excel(writer, sheet_name=abc_sheetname)
        d_test_name = 'D_test_results'
        d_df.to_excel(writer, sheet_name=d_test_name)
        displ_cols = [col for col in d_df.columns if '_Displacement' in col]
        force_cols = [col for col in d_df.columns if '_Force' in col]
        displ_cols.sort()
        force_cols.sort()
        returnDict = {}
        for f, d in zip(force_cols, displ_cols):
            idx = d_df[[f]].idxmax(axis=0)
            v = d_df.iloc[idx][d]
            val = d_df[[f]].max(axis=0)
            avg = d_df[[f]].mean(axis=0)
            outL = [round(avg[0], 2), val[0], v.values[0]]
            returnDict[f + 'Results'] = outL
        returnDict['Results_Index'] = ['Average Force (N)', 'Maximum Force (N)', 'Displacement @ Max Force (mm)']
        res = pd.DataFrame(returnDict)
        res.to_excel(writer, sheet_name=d_test_name, startcol=11)
        writer.save()
        return 0

    def wetTest_dry(self, in_queue, out_queue, testSpecs):
        out_queue.put(["Dry Pretest"],block = False)
        logging.info("Dry Pretest")
        print("Setting Leak Tester")
        combineDispDf = None
        combineSealDf = None
        combineAspDf = None
        ret = self.isaacStaticSealInit(testSpecs.staticSealSpec, testSpecs.staticTime)
        disp_spds = testSpecs.wetDry_dispSpds
        staticSealPos = testSpecs.wetDry_sealpos
        asp_spd = testSpecs.wetDry_aspSpd
        ret = self.check_rdy()
        self.mk_ten.write(str.encode('Z\r\n'))
        home = self.zHOME()
        print("Find Plunger")
        found = self.findPlunger()
        i = 0
        leakDict = {}
        self.checkPauseQue(in_queue)
        num_test = len(disp_spds)
        for spd in disp_spds:
            print("Force Dispense")
            out_queue.put(["Force Dispense 21->0, %s mm/s.  Rem. Test: %s"%(spd, num_test-i)],block = False)
            logging.info("Force Dispense 21->0, %s mm/s"%spd)
            distDisp, forceDisp = self.forceDispense(21, spd)
            self.checkPauseQue(in_queue)
            print("Speed Aspirate")
            out_queue.put(["Speed Aspirate 0->20 4.81 mm/s. Rem. Test: %s"%(num_test-i)],block = False)
            logging.info("Speed Aspirate 0->20, 4.81 mm/s")
            distAsp, forceAsp = self.speedAspirateTest(20, asp_spd)
            self.checkPauseQue(in_queue)
            ret = self.check_rdy()
            r = self.pulseOPcwFast(20) #pos = 0
            print("Static Seal")
            out_queue.put(["Static Seal Testing. Rem. Test: %s"%(num_test-i)],block = False)
            logging.info("Static Seal Testing")
            self.checkPauseQue(in_queue)
            staticSealPos.sort()
            ret = self.check_rdy()
            itt = 0
            pos = 0
            leakDict["Test #"] = str(i)
            for posL in staticSealPos:
                ret = self.checkPauseQue(in_queue)
                logging.info(
                    "Dry Pretest, Individual Test: %s, Static Seal Pos %s" % (
                    str(i), str(posL)))
                movePulse = posL - pos
                ret = self.pulseOPccwFast(movePulse)
                pos += movePulse
                result, leak = self.sealTest_static()
                leakDict["Static Seal Result @ " + str(posL) + "mm"] = result
                leakDict["Static Seal Leak Amount @" + str(posL) + "mm (psig)"] = leak
                itt += 1
            print("exit Static Seal")
            logging.info("Exit Static Seal")
            print("Data Cleanup")
            forceDispRes = np.vstack((distDisp, forceDisp))
            forceAspRes = np.vstack((distAsp, forceAsp))
            if combineDispDf is None:
                t1_dispdf, t1_sealTest, t1_aspdf = self.dataOut_makedf(i, forceDispRes, leakDict, forceAspRes)
                combineDispDf, combineSealDf, combineAspDf = self.dataOut_makedf(i, forceDispRes, leakDict,
                                                                                 forceAspRes)
            else:
                t1_dispdf, t1_sealTest, t1_aspdf = self.dataOut_makedf(i, forceDispRes, leakDict, forceAspRes)
                combineDispDf, combineSealDf, combineAspDf = self.combine_DataFrames(t1_dispdf, t1_sealTest, t1_aspdf, combineDispDf, combineSealDf, combineAspDf)
            i+=1
            ret = self. pulseOPccwFastEx(1)
        r = self.check_rdy()
        out_queue.put(["Data Output, Prepare to Prime"])
        ret = self.pulseOPcwFastEx(21.5)
        self.makeExcelOut(testSpecs.partID, "Dry_Pretest", combineDispDf, combineSealDf, combineAspDf)
        App.get_running_app().root.ids.sm.current = 'wet_test'
        return 0

    def wetTest_prime(self, in_queue, out_queue):
        print("Wet Test Priming")
        logging.info("Wet Test Priming")
        out_queue.put(["Wet Test Priming"],block = False)
        for i in range(3):
            self.checkPauseQue(in_queue)
            out_queue.put(["Wet Test Priming, %s cycles remain"%(2-i)], block=False)
            self.check_rdy()
            ret = self.pulseOPccw(21.5, 0.05)
            self.check_rdy()
            self.checkPauseQue(in_queue)
            ret = self.pulseOPcw(21.5, 0.48)
        self.check_rdy()
        self.pulseOPccwFastEx(0.5)
        out_queue.put(["Wet Test Ready"], block=False)
        App.get_running_app().root.ids.sm.current = 'wet_test'
        print("Exit Prime")

    def wetTest_workflow(self, in_queue, out_queue, testSpecs):
        """Performs Full Wet Test"""
        repeat = testSpecs.wetTest_repeat
        print("Resume Wet Test Workflow")
        out_queue.put(["Resume Wet Test Workflow"], block = False)
        logging.info("Resume Wet Test Workflow")
        for i in range(int(repeat)):
            abc_comb_df = self.wetTest_abc(in_queue, out_queue, testSpecs, i)
            d_df = self.wetTest_d(in_queue, out_queue, i)
            t_w0 = threading.Thread(target=self.wetTest_dfOut, args = (abc_comb_df, d_df, i, testSpecs))
            t_w0.daemon = True
            t_w0.start()
            App.get_running_app().root.ids.sm.get_screen('WetRun').ids.remTestCyc.text = str(repeat-(i+1))
            rdy = self.check_rdy()
            r = self.pulseOPccwFast(20)
            rdy = self.check_rdy()
            r = self.speedFullStrokeParkTest(20, int(testSpecs.wetTest_recip), testSpecs.recipSpeed, 0.1, testSpecs.wetTest_parkpos, in_queue, out_queue, i)
            r = self.pulseOPcwFastEx(20)
        #ends at top pos = 20
        r = self.pulseOPcwFastEx(0.5)
        print("Wet Test Complete")
        logging.info("Wet Test Complete.  Unload DPV TestSetup")
        out_queue.put(["Wet Test Complete, Unload DPV TestSetup"], block=False)
        App.get_running_app().root.ids.sm.current = 'wet_test'
        return 0


class testSpecs:
    def __init__(self, partID='000', parkTlen=2.0, parkPos=None, test=None, otfNumCycles=5, otftestTime=60.0,
                 staticSealSpec=0.2, otfSealSpec=2.6, speed=4.81, speedPos=None, recipCycles=1, testRepeat=2):
        # PD
        self.staticTime = 6.0
        self.staticSealSpec = staticSealSpec
        self.otfSealSpec = otfSealSpec
        self.otfNumCycles = otfNumCycles
        self.otftestTime = otftestTime
        self.parkPos = [[15.0]]  # [[0,5,10,15,20,25]]
        # speed
        self.test = [4.81]
        self.recipSpeed = 4.81
        self.aspSpeed = 4.81
        # TC
        self.partID = partID
        self.strokeDist = 25.0
        self.parkTlen = parkTlen
        self.speedPos = [0,4,8,12,16,20]
        self.recipCycles = recipCycles
        self.testRepeat = testRepeat
        #WetTestVars
        self.wetDry_sealpos = [0,4,8,12,16,20]
        self.wetDry_dispSpds = [0.03, 0.77, 2.41]
        self.wetDry_aspSpd = 4.81
        self.wetTest_aspSpds = [0.03, 0.77, 2.41]
        self.wetTest_dispSpd = 4.81
        self.wetTest_parkpos = [0,4,8,12,16,20]
        self.wetTest_repeat = 10
        self.wetTest_recip = 196

    def printSelf(self):
        print(self.partID, self.strokeDist, self.parkPos,
              self.parkTlen, self.test,
              self.otfSealSpec, self.otfNumCycles,
              self.otftestTime, self.staticSealSpec, self.staticTime,
              self.aspSpeed, self.recipSpeed, self.speedPos,
              self.recipCycles, self.testRepeat)
        logging.info("Test Params Confirmed")
        logging.info(self.partID, self.strokeDist, self.parkPos,
                     self.parkTlen, self.test,
                     self.otfSealSpec, self.otfNumCycles,
                     self.otftestTime, self.staticSealSpec, self.staticTime,
                     self.aspSpeed, self.recipSpeed, self.speedPos,
                     self.recipCycles, self.testRepeat)


class SideScreenManager(ScreenManager):
    pass


class Connection(Screen):
    ardComSelect = None
    mktenComSelect = None
    isaacIP = None
    entrisComSelect = None
    nocomstr = ['No COM Available']
    ports = ListProperty()

    def __init__(self, **kwargs):
        super(Connection, self).__init__(**kwargs)
        self.ports = self.refreshCOMports()

    def spinSelect(self, value):
        if value == self.nocomstr[0]:
            return 0
        else:
            return 1

    def refreshCOMports(self):
        print("Refresh COM Ports")
        logging.info("Refreshing COM Ports")
        ports = serial.tools.list_ports.comports()
        devices = []
        for comport in ports:
            devices.append(comport.device)
        if devices == []:
            return self.nocomstr
        else:
            self.ports = tuple(devices)
            return tuple(devices)

    def testArdThread(self):
        t0 = threading.Thread(target=self.testArdfxn)
        t0.daemon = True
        t0.start()

    def testArdfxn(self):
        print("testArd")
        logging.info("Test Arduino Function")
        app = App.get_running_app()
        tA = app.root.testArea

        if tA.ard == None:
            self.ardComSelect = None
            app.root.ids.ActionBar.ids.ardCon.text = 'Failed'
            logging.info("No Arduino Connection.  Ard == None")
        else:
            tA.jog_fwd(1000)
            tA.zHOME()
            app.root.ids.ActionBar.ids.ardCon.text = 'Connected'
            logging.info("Ard Connected")

    def testMktenfxnThread(self):
        t4 = threading.Thread(target=self.testMktenfxn)
        t4.daemon = True
        t4.start()

    def testMktenfxn(self):
        print("testmk10")
        logging.info("Test Mk10 Function")
        app = App.get_running_app()
        tA = app.root.testArea
        if tA.mk_ten == None:
            self.mktenComSelect = None
            app.root.ids.ActionBar.ids.mktenCon.text = 'Failed'
            logging.info("Mk10 Connection Failed.  mk_ten == None")
        else:
            ret = tA.readMk10()
            if ret is not None:
                app.root.ids.ActionBar.ids.mktenCon.text = 'Connected'
                logging.info("Mk-10 Connected")
            else:
                app.root.ids.ActionBar.ids.mktenCon.text = 'Failed'
                logging.info("Mk-10 Connection failed, Cannot read from serial")

    def testIsaacfxn(self):
        print("testisaac")
        logging.info("Test IsaacHD Function")
        app = App.get_running_app()
        tA = app.root.testArea
        if tA.isaac == None:
            self.isaacIP = None
            app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
            logging.info("Test IsaacHD function failed.  isaac == None")
        else:
            ret = tA.isaacChangeProgram('02')
            if ret != None:
                app.root.ids.ActionBar.ids.isaacCon.text = 'Connected'
                logging.info("IsaacHD connected")
            else:
                app.root.ids.ActionBar.ids.isaacCon.text = 'Failed'
                logging.info("IsaacHD Connection failed.  Cannot change program")

    def testIsaacfxnThread(self):
        t6 = threading.Thread(target=self.testIsaacfxn)
        t6.daemon = True
        t6.start()

    def testEntrisfxn(self):
        print("testEntris")
        logging.info("Test Entris Function")
        app = App.get_running_app()
        tA = app.root.testArea
        if tA.entris is None:
            self.entrisComSelect = None
            app.root.ids.ActionBar.ids.entrCon.text = 'Failed'
            logging.info("Entris Connection Failed.  entris == None")
        else:
            tA.readEntris()
            ret = tA.retr_entr_out()
            if ret[1] is not None:
                print(ret)
                app.root.ids.ActionBar.ids.entrCon.text = 'Connected'
                logging.info("Entris Connected")
                tA.clear_entr_queue()
            else:
                app.root.ids.ActionBar.ids.entrCon.text = 'Failed'
                logging.info("Entris Connection Failed, Cannot read from serial")

    def testEntrisfxnThread(self):
        t_13 = threading.Thread(target=self.testEntrisfxn)
        t_13.daemon = True
        t_13.start()

    def ardConThread(self):
        t0 = threading.Thread(target=self.initArdConnect)
        t0.daemon = True
        t0.start()

    def initArdConnect(self):
        print(self.ardComSelect)
        App.get_running_app().root.testArea.ard = str(self.ardComSelect)
        logging.info("Setting ard @ COM = %s" % self.ardComSelect)

    def mktenConThread(self):
        t1 = threading.Thread(target=self.initMktenConnect)
        t1.daemon = True
        t1.start()

    def initMktenConnect(self):
        print(self.mktenComSelect)
        App.get_running_app().root.testArea.mk_ten = self.mktenComSelect
        logging.info("Setting mkten @ COM = %s" % self.mktenComSelect)

    def isaacConThread(self):
        t2 = threading.Thread(target=self.initisaacConnect)
        t2.daemon = True
        t2.start()

    def initisaacConnect(self):
        print(self.isaacIP)
        App.get_running_app().root.testArea.isaac = self.isaacIP
        logging.info("Setting isaac @ IP = %s" % self.isaacIP)

    def entrisConThread(self):
        t_12 = threading.Thread(target=self.initEntrisConnect)
        t_12.daemon = True
        t_12.start()

    def initEntrisConnect(self):
        print(self.entrisComSelect)
        App.get_running_app().root.testArea.entris = self.entrisComSelect
        logging.info("Setting entris @ COM = %s" % self.entrisComSelect)


class DebugScreen(Screen):
    alarmState = None
    manual_control = False
    pulseOpDist = None
    pulseOpSpd = None

    # functions comment blocked for now
    def jogDown(self):
        print("JogFwdToggleOn")
        App.get_running_app().root.testArea.toggleFWDjog()
        logging.info("Debug - Toggle Jog Down")

    def jogUp(self):
        print("JogBackToggleOn")
        App.get_running_app().root.testArea.toggleREVjog()
        logging.info("Debug - Toggle Jog Up")

    def homeThread(self):
        t3 = threading.Thread(target=self.zHome)
        t3.daemon = True
        t3.start()

    def zHome(self):
        print("zHOME")
        App.get_running_app().root.testArea.zHOME()
        logging.info("Debug - ZHOME")

    def findPungerThread(self):
        t1 = threading.Thread(target=self.findPlunger)
        t1.daemon = True
        t1.start()

    def findPlunger(self):
        print("FindPlunger")
        self.ids.fPlung.state = 'down'
        logging.info("Debug -  Start Find Plunger")
        App.get_running_app().root.testArea.findPlunger()
        logging.info("Debug - Plunger Found")
        self.ids.fPlung.state = 'normal'

    def stop(self):
        print("STOP!")
        App.get_running_app().root.testArea.toggleSTOP()
        logging.info("Debug - Toggle Stop")

    def free(self):
        print("Free")
        App.get_running_app().root.testArea.toggleFree()
        logging.info("Debug - Toggle Free")

    def checkAlarm(self):
        print("Check Alarm")
        app = App.get_running_app().root
        logging.info("Debug - Check Alarm")
        if app.testArea.checkALRM():
            self.alarmState = True
            logging.info("Debug - Alarm!")
            self.ids.alrmSts.text = 'Alarm! Reset!'
        else:
            self.alarmState = False
            logging.info("Debug - No Alarm")
            self.ids.alrmSts.text = 'No Alarm'

    def resetAlarm(self):
        print("Clear Alarm")
        logging.info("Debug - Reset Alarm")
        if App.get_running_app().root.testArea.resetALRM():
            # error
            logging.info("Clear Alarm Error. Use Free then Cycle Power")
        else:
            self.alarmState = False
            self.ids.alrmSts.text = 'Alarm has been Reset'
            logging.info("Deubg - Alarm has been reset")

    def parsDist(self, raw_distance):
        if raw_distance[0] == '+':
            direction = 1
        elif raw_distance[0] == '-':
            direction = 2
        else:
            return 0, 0
        dist = float(raw_distance[1:])
        if dist > 400:
            return 0, 0
        else:
            return direction, dist

    def executeDebugThread(self):
        t2 = threading.Thread(target=self.execute)
        t2.daemon = True
        t2.start()

    def execute(self):
        app = App.get_running_app().root
        self.ids.execute.state = 'down'
        speed = float(self.pulseOpSpd)
        direction, distance = self.parsDist(self.pulseOpDist)
        logging.info("Debug - Pulse Operation - %s Direction - %s Distance" % (str(direction), str(distance)))
        if direction == 0:
            print("Popup")
            print(direction, distance, speed)
        elif direction == 2:
            print(direction, distance, speed)
            # pulse op counterclockwise
            if speed < 2:
                app.testArea.pulseOPcw(distance, speed)
            elif speed < 4:
                app.testArea.pulseOPcwFastEx(distance)
            else:
                app.testArea.pulseOPcwFast(distance)
        else:
            print(direction, distance, speed)
            # pulse op cw
            if speed < 2:
                app.testArea.pulseOPccw(distance, speed)
            elif speed < 4:
                app.testArea.pulseOPccwFastEx(distance)
            else:
                app.testArea.pulseOPccwFast(distance)
        self.ids.execute.state = 'normal'

    def statTestThread(self):
        app = App.get_running_app().root
        t9 = threading.Thread(target=self.defaultStaticTest)
        t9.daemon = True
        t9.start()

    def defaultStaticTest(self):
        app = App.get_running_app().root
        logging.info("Default Static Test")
        result, leak = app.testArea.sealTest_static()
        self.ids.dbRes.text = result
        self.ids.dbLeak.text = str(leak)

    def otfTestThread(self):
        t10 = threading.Thread(target=self.defaultOtfTest)
        t10.daemon = True
        t10.start()

    def defaultOtfTest(self):
        app = App.get_running_app().root
        logging.info("Default OTF Test")
        result, leak = app.testArea.sealTest_OnTheFly(5, 60, 20)
        self.ids.dbRes.text = result
        self.ids.dbLeak.text = str(leak)

    def entris_tare_thread(self):
        t_14 = threading.Thread(target=self.entrisTare)
        t_14.daemon = True
        t_14.start()

    def entrisTare(self):
        app = App.get_running_app().root
        logging.info("Debug Entris Balance Tare")
        app.testArea.tareEntris()
        self.ids.entRes.text = "Tare Balance"

    def entris_read_thread(self):
        t_15 = threading.Thread(target=self.entrisRead)
        t_15.daemon = True
        t_15.start()

    def entrisRead(self):
        app = App.get_running_app().root
        logging.info("Debug Entris Balance Read")
        result = app.testArea.readEntris()
        if result[0] is None:
            self.ids.entRes.text = result[1] + " " + result[2]
        else:
            self.ids.entRes.text = result[0] + " " + result[1] + " " + result[2]


class TestBuildScreen(Screen):
    # global
    # PD
    staticTime = 6.0
    staticSpec = 0.02
    otfSealSpec = 2.6
    otfNumCycles = 5
    otftestTime = 60.0
    parkPos = [[0.0, 5.0, 10.0, 15.0, 20.0, 25.0]]
    # speed
    test = []  # Dispense speed
    curTestCycles = ""  # append str()+\n later .split'\n'
    recip = 4.81
    aspSpd = 4.81

    # during recip full stroke
    speedPos = [0, 4, 8, 12, 16, 20]
    partID = '000'  # ### of test
    parkTlen = 1.5
    strokeDist = 21.00
    # number of test repeats
    testRepeat = 10
    # number of recips
    recip_cycles = 200
    # total Cycles
    total_cycles = testRepeat * recip_cycles

    def refreshTest(self, t):
        old_str = self.curTestCycles
        if old_str == "":
            new_str = t
        else:
            new_str = old_str + "\n" + t
        self.curTestCycles = new_str

    def addTestCycle(self, new_test_speed):
        t = float(new_test_speed)
        self.test.append(t)
        self.refreshTest(new_test_speed)
        ###parse text entry, list the texxt append to self.parkPos

    def clearTests(self):
        self.test = []
        self.curTestCycles = ""

    def addAspSpeed(self, aspSpd):
        self.aspSpd = float(aspSpd)

    def pars_spec(self, textInput):
        return textInput

    def exec_Event(self):
        App.get_running_app().root.ids.sm.get_screen('TestRun').testRun = True
        self.executeButtonThread()

    def setTestspecs(self):
        testSpecs = App.get_running_app().root.testSpecs
        # TC
        testSpecs.partID = self.partID
        testSpecs.strokeDist = self.strokeDist
        testSpecs.parkTlen = self.parkTlen
        testSpecs.speedPos = self.speedPos
        testSpecs.recipCycles = self.recip_cycles
        testSpecs.testRepeat = self.testRepeat
        # Speed
        testSpecs.test = self.test
        testSpecs.recipSpeed = self.recip
        testSpecs.aspSpeed = self.aspSpd
        # PD
        testSpecs.staticTime = self.staticTime
        testSpecs.staticSealSpec = self.staticSpec
        testSpecs.otfSealSpec = self.otfSealSpec
        testSpecs.otftestTime = self.otftestTime
        testSpecs.otfNumCycles = self.otfNumCycles
        testSpecs.parkPos = self.parkPos
        testSpecs.printSelf()

        # testRunScreen
        testRScreen = App.get_running_app().root.ids.sm.get_screen('TestRun')

        testRScreen.staticTime = str(self.staticTime)
        testRScreen.staticSpec = str(self.staticSpec)
        testRScreen.otfSealSpec = str(self.otfSealSpec)
        testRScreen.otfNumCycles = str(self.otfNumCycles)
        testRScreen.otftestTime = str(self.otftestTime)
        testRScreen.parkPos = ''.join((str(e) + ", ") for e in self.parkPos[0])

        testRScreen.curTestCycles = self.curTestCycles.replace('\n', ', ')
        testRScreen.recip = str(self.recip)
        testRScreen.aspSpd = str(self.aspSpd)

        testRScreen.partID = self.partID
        testRScreen.parkTlen = str(self.parkTlen)
        testRScreen.strokeDist = str(self.strokeDist)
        testRScreen.testRepeat = str(self.testRepeat)
        testRScreen.recip_cycles = str(self.recip_cycles)

    def confirmParams(self):
        ids = self.ids
        # strokeDist
        self.strokeDist = float(ids.strokeDist.text)

        # PD specs
        self.staticSpec = float(ids['staticSpec'].text)
        self.staticTime = float(ids['staticTime'].text)
        self.otfSealSpec = float(ids['otfSpec'].text)
        self.otftestTime = float(ids['otfTime'].text)
        self.otfNumCycles = int(ids['otfCycles'].text)

        # static Seal Positions
        parkPos = []
        finalpos = []
        parkPos.append(ids['pos1'].text)
        parkPos.append(ids['pos2'].text)
        parkPos.append(ids['pos3'].text)
        parkPos.append(ids['pos4'].text)
        parkPos.append(ids['pos5'].text)
        parkPos.append(ids['pos6'].text)
        for pos in parkPos:
            if pos == "":
                parkPos.remove(pos)
            elif float(pos) < 0:
                parkPos.remove(pos)
            elif float(pos) > self.strokeDist:
                parkPos.remove(pos)
            else:
                finalpos.append(float(pos))
        self.parkPos[0] = finalpos

        # LC
        self.partID = ids['partID'].text
        self.parkTlen = float(ids['parkTimeLC'].text)
        self.testRepeat = int(ids['testRepeat'].text)
        self.recip_cycles = int(ids['recipCycleTC'].text)
        self.total_cycles = self.testRepeat * self.recip_cycles
        ids['totalCycles'].text = str(self.total_cycles)

        self.setTestspecs()

    def executeButtonThread(self):
        t = threading.Thread(target=self.execute)
        t.daemon = True
        t.start()

    def execute(self):
        roo = App.get_running_app().root
        roo.testArea.run_Spec(roo.testSpecs, roo.in_queue, roo.out_queue)


class TestRunScreen(Screen):
    # PD
    staticTime = ""
    staticSpec = ""
    otfSealSpec = ""
    otfNumCycles = ""
    otftestTime = ""
    parkPos = ""
    # speed
    curTestCycles = ""  # append str()+\n later .split'\n'
    recip = ""
    aspSpd = ""

    # during recip full stroke
    partID = '000'  # ### of test
    parkTlen = ""
    strokeDist = ""
    # number of test repeats
    testRepeat = ""
    # number of recips
    recip_cycles = ""
    # total Cycles
    testRun = False

    def printSelf(self):
        print(self.partID, self.strokeDist, self.parkPos,
              self.parkTlen, self.curTestCycles,
              self.otfSealSpec, self.otfNumCycles,
              self.otftestTime, self.staticSpec, self.staticTime,
              self.aspSpd, self.recip,
              self.recip_cycles, self.testRepeat)

    def refreshParams(self):
        label1 = "Test Parameters:\n"
        label1 += ("Part ID:             " + self.partID + "\n")
        label1 += ("Stroke Distance:     " + self.strokeDist + "\n")
        label1 += ("Test Repeats:        " + self.testRepeat + "\n")
        label1 += ("Reciprocation Cycles:     " + self.recip_cycles + "\n")
        label1 += ("     Moving at:      " + self.recip + " mm/s\n")
        label1 += ("     Parking for:    " + self.parkTlen + " s\n")
        label1 += ("Dispense Speeds (mm/s):     " + self.curTestCycles + "\n")
        label1 += ("Aspiration Speed (mm/s):    " + self.aspSpd + "\n")
        label1 += ("Static Seal:\n")
        label1 += ("     Spec (psi):     " + self.staticSpec + "\n")
        label1 += ("     Test Time (s):  " + self.staticTime + "\n")
        label1 += ("On The Fly Seal:\n")
        label1 += ("     Spec (psi):     " + self.otfSealSpec + "\n")
        label1 += ("     Test Time (s):  " + self.otftestTime + "\n")
        label1 += ("     Reciprocations: " + self.otfNumCycles + "\n")
        App.get_running_app().root.ids.sm.get_screen('TestRun').ids.param1.text = label1

    def pauseTestThread(self):
        roo = App.get_running_app().root
        if self.testRun == False:  # test is not running, needs to unpause
            roo.in_queue.put(False, block=False)
            print("Resuming")
            self.testRun = True
        else:  # test is running, needs to pause
            roo.in_queue.put(True, block=False)
            print("Pausing")
            self.testRun = False

    def emergencyStop(self):
        roo = App.get_running_app().root
        print("Exit Start")
        roo.in_queue.put("exit", block=False)
        roo.testArea.ard.write(b'!')
        print("Blocking")
        while not roo.in_queue.empty():
            print("waiting")
            time.sleep(5)
        roo.testArea.ard.write(b'!')
        roo.testArea.zHOME()
        roo.ids.sm.current = "build_test"
        roo.ids.sm.get_screen('TestRun').ids.stopButton.state = 'normal'

    def emergencySTOPthread(self):
        st = threading.Thread(target=self.emergencyStop)
        st.daemon = True
        st.start()

    def updateStatus(self):
        testRunScreen = App.get_running_app().root.ids.sm.get_screen('TestRun')
        status_string = None
        out_test = None
        in_test = None
        test_prog = None
        totalTests = len(App.get_running_app().root.testSpecs.test)
        while True:
            try:
                packet = App.get_running_app().root.out_queue.get(block=False)
                # parse packet
                status_string = packet[0]
                out_test = packet[1]
                in_test = packet[2]
                test_prog = packet[3]
            except queue.Empty:
                time.sleep(5)
                continue
            if status_string is None:
                continue
            elif out_test is None:
                testRunScreen.ids.testStatusText.text = status_string
                testRunScreen.ids.remTestCyc.text = self.testRepeat
                testRunScreen.ids.fullStRem.text = str(int(self.recip_cycles) * int(self.testRepeat))
            else:
                testRunScreen.ids.testStatusText.text = status_string
                testRunScreen.ids.remTestCyc.text = str(int(self.testRepeat) - out_test)
                testRunScreen.ids.fullStRem.text = str((int(self.testRepeat) - (out_test - 1)) * int(self.recip_cycles))
                if status_string == "Beginning Testing":
                    testRunScreen.ids.cycleNum.value = 0
                if in_test == 90:
                    totalcycles = int(self.recip_cycles)
                    testRunScreen.ids.fullStRem.text = str(
                        (int(self.testRepeat) - (out_test - 1)) * int(self.recip_cycles) - (test_prog + 1))
                    testRunScreen.ids.cycleNum.value_normalized = test_prog / totalcycles
                elif in_test == 100:
                    testRunScreen.ids.fullStRem.text = "0"
                    testRunScreen.ids.remTestCyc.text = "0"
                else:  # in_test
                    print(totalTests, in_test, test_prog)
                    section = ((in_test - 1) / totalTests) * 100
                    inTestProg = test_prog / 5
                    norm = 100 / totalTests
                    norm_prog = inTestProg * norm
                    testbarUpdate = section + norm_prog
                    testRunScreen.ids.testStatus.value = testbarUpdate

    def updateStatusThread(self):
        prog = threading.Thread(target=self.updateStatus)
        prog.daemon = True
        prog.start()


class WetTestScreen(Screen):
    ops_spd = None
    ops_dist = None
    dry_test_spds = [0.33,0.77,2.41]
    wet_test_spds = [0.33, 0.77, 2.41]
    curTestCycles = "0.03\n0.77\n2.41"  # append str()+\n later .split'\n'
    curwetCycles = "0.03\n0.77\n2.41"
    wet_test_tot_cyc = 2000
    wet_pid = ""
    seal_pos = [0, 4, 8, 12, 16, 20]
    def aspirateTest(self, dist, speed):
        tA = App.get_running_app().root.testArea
        print("Aspirate Test %s mm, %s mm/s" % (dist, speed))
        tim, vols = tA.aspirate_wet_test(float(dist), float(speed))
        fvol, avgfr, frA = tA.calcflow_rate(tim, vols)
        print(fvol)
        print(avgfr)

    def dispenseTest(self, dist, speed):
        tA = App.get_running_app().root.testArea
        print("Dispense Test %s mm, %s mm/s" % (dist, speed))
        tim, vols = tA.dispense_wet_test(float(dist), float(speed))
        fvol, avgfr, frA = tA.calcflow_rate(tim, vols)
        print(fvol)
        print(avgfr)

    def disp_thread(self):
        t_21 = threading.Thread(target=self.dispenseTest, args=(self.ops_dist, self.ops_spd))
        t_21.daemon = True
        t_21.start()

    def asp_thread(self):
        t_22 = threading.Thread(target=self.aspirateTest, args=(self.ops_dist, self.ops_spd))
        t_22.daemon = True
        t_22.start()

    def addTestCycle(self, new_test_speed):
        t = float(new_test_speed)
        self.dry_test_spds.append(t)
        self.refreshTest(new_test_speed)
        ###parse text entry, list the texxt append to self.parkPos

    def refreshTest(self, t):
        old_str = self.curTestCycles
        if old_str == "":
            new_str = t
        else:
            new_str = old_str + "\n" + t
        self.curTestCycles = new_str

    def clearTests(self):
        self.dry_test_spds = []
        self.curTestCycles = ""

    def confirm_dry(self):
        ids = self.ids
        ts = App.get_running_app().root.testSpecs

        ts.wetDry_dispSpds = self.dry_test_spds
        ts.partID = ids['pid'].text
        ids['w_pid'].text = ids['pid'].text

        ts.staticTime = 6.0
        ts.staticSealSpec = 0.2

        new_seal_pos = []
        new_seal_pos.append(float(ids['pos0'].text))
        new_seal_pos.append(float(ids['pos1'].text))
        new_seal_pos.append(float(ids['pos2'].text))
        new_seal_pos.append(float(ids['pos3'].text))
        new_seal_pos.append(float(ids['pos4'].text))
        new_seal_pos.append(float(ids['pos5'].text))
        self.seal_pos = new_seal_pos
        ts.wetDry_sealpos = self.seal_pos
        ts.wetDry_aspSpd = ids['pd_aspspd'].value

        print(ts.wetDry_aspSpd, ts.wetDry_dispSpds, ts.wetDry_sealpos, ts.partID)

    def dry_thread(self):
        root = App.get_running_app().root
        t_w1 = threading.Thread(target=root.testArea.wetTest_dry, args=(root.in_queue, root.out_queue, root.testSpecs))
        t_w1.daemon = True
        t_w1.start()

    def waddTestCycle(self, new_test_speed):
        t = float(new_test_speed)
        self.wet_test_spds.append(t)
        self.wrefreshTest(new_test_speed)
        ###parse text entry, list the texxt append to self.parkPos

    def wrefreshTest(self, t):
        old_str = self.curwetCycles
        if old_str == "":
            new_str = t
        else:
            new_str = old_str + "\n" + t
        self.curwetCycles = new_str

    def wclearTests(self):
        self.wet_test_spds = []
        self.curwetCycles = ""

    def confirm_wet(self):
        ts = App.get_running_app().root.testSpecs
        ids = self.ids

        ts.wetTest_aspSpds = self.wet_test_spds
        ts.wetTest_dispSpd = ids['wet_disp_spd'].value
        ts.wetTest_recip = float(ids['wet_recip'].text)
        ts.wetTest_repeat = float(ids['wet_test_num'].text)
        total_cycles = (int(ids['wet_recip'].text)+4)*int(ids['wet_test_num'].text)
        ids['wet_test_total_cycles'].text = str(total_cycles)

        print(ts.wetTest_aspSpds, ts.wetTest_dispSpd, ts.wetTest_recip, ts.wetTest_repeat)

    def wet_thread(self):
        root = App.get_running_app().root
        t_w2 = threading.Thread(target=root.testArea.wetTest_workflow, args = (root.in_queue, root.out_queue, root.testSpecs))
        t_w2.daemon = True
        t_w2.start()

    def prime_thread(self):
        root = App.get_running_app().root
        t_w3 = threading.Thread(target=root.testArea.wetTest_prime,
                                args=(root.in_queue, root.out_queue))
        t_w3.daemon = True
        t_w3.start()

    def zhomeThread(self):
        tw_3 = threading.Thread(target=App.get_running_app().root.testArea.zHOME)
        tw_3.daemon = True
        tw_3.start()


class WetRunScreen(Screen):

    testRun = False

    def pauseTestThread(self):
        roo = App.get_running_app().root
        if self.testRun == False:  # test is not running, needs to unpause
            roo.in_queue.put(False, block=False)
            print("Resuming")
            self.testRun = True
        else:  # test is running, needs to pause
            roo.in_queue.put(True, block=False)
            print("Pausing")
            self.testRun = False

    def emergencyStop(self):
        roo = App.get_running_app().root
        print("Exit Start")
        roo.in_queue.put("exit", block=False)
        roo.testArea.ard.write(b'!')
        print("Blocking")
        while not roo.in_queue.empty():
            print("waiting")
            time.sleep(5)
        roo.testArea.ard.write(b'!')
        roo.testArea.zHOME()
        roo.ids.sm.current = "wet_test"
        roo.ids.sm.get_screen('TestRun').ids.stopButton.state = 'normal'

    def emergencySTOPthread(self):
        st = threading.Thread(target=self.emergencyStop)
        st.daemon = True
        st.start()

    def updateStatus(self):
        wetRunScreen = App.get_running_app().root.ids.sm.get_screen('WetRun')
        status_string = None
        out_test = None
        in_test = None
        test_prog = None
        while True:
            try:
                packet = App.get_running_app().root.out_queue.get(block=False)
            except queue.Empty:
                time.sleep(5)
                continue
            try:
                # parse packet
                status_string = packet[0]
                out_test = packet[1]
                in_test = packet[2]
                test_prog = packet[3]
            except IndexError:
                if status_string is None:
                    continue
                else:
                    wetRunScreen.ids.testStatusText.text = status_string
                    continue
            wetRunScreen.ids.testStatusText.text = status_string
            wetRunScreen.ids.fullStRem.text = str(196 - (test_prog + 1))

    def updateStatusThread(self):
        prog = threading.Thread(target=self.updateStatus)
        prog.daemon = True
        prog.start()

    def drefresh_param_info(self):
        ts = App.get_running_app().root.testSpecs

        params = "Dry Test Parameters:\n"
        params += ("Part ID:             " + ts.partID + "\n")
        params += ("Dry Dispense Speeds (mm/s):     " + ''.join(str(e)+',' for e in ts.wetDry_dispSpds) + "\n")
        params += ("Dry Aspiration Speed (mm/s):    " + str(ts.wetDry_aspSpd) + "\n")
        params += ("Static Seal:\n")
        params += ("     Spec (psi):     " + str(ts.staticSealSpec) + "\n")
        params += ("     Test Time (s):  " + str(ts.staticTime) + "\n")
        App.get_running_app().root.ids.sm.get_screen('WetRun').ids.wet_param.text = params

    def wrefresh_param_info(self):
        ts = App.get_running_app().root.testSpecs

        params = "Wet Test Parameters:\n"
        params += ("Part ID:             " + ts.partID + "\n")
        params += ("Wet Dispense Speed (mm/s):     " + str(ts.wetTest_dispSpd) + "\n")
        params += ("Wet Aspiration Speeds (mm/s):    " + ''.join(str(e) + ',' for e in ts.wetTest_aspSpds) + "\n")
        params += ("Wet Force Aspirate/Dispense Speed (mm/s):     "+str(ts.wetTest_dispSpd)+"\n")
        params += ("Test Repeats:        " + str(ts.wetTest_repeat) + "\n")
        params += ("Reciprocation Cycles:     " + str(ts.wetTest_recip) + "\n")
        params += ("     Moving at:      " + str(ts.recipSpeed) + " mm/s\n")
        params += ("     Parking for:    " + "0.1 s\n")
        App.get_running_app().root.ids.sm.get_screen('WetRun').ids.wet_param.text = params

    def prefresh_param_info(self):
        params = "Priming Parameters:\n"
        params += "Full Stroke Distance:     21.5 mm\n"
        params += "Priming Aspiration Speed:     0.05 mm/s\n"
        params += "Priming Dispense Speed:     0.48 mm/s\n"
        params += "Full Cycle Repeats:     3\n"
        App.get_running_app().root.ids.sm.get_screen('WetRun').ids.wet_param.text = params




class ModuleSelect(BoxLayout):
    in_queue = queue.Queue()
    out_queue = queue.Queue()
    testArea = testArea()
    testSpecs = testSpecs()
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logging.basicConfig(filename="DPV_Tester.log",
                        filemode='a',
                        format='%(asctime)s : %(msecs)d - %(levelname)s - %(message)s',
                        datefmt='%Y-%m-%d -- %H : %M : %S',
                        level=logging.INFO)

    def __init__(self, **kwargs):
        super(ModuleSelect, self).__init__(**kwargs)

    def popupManual(self):
        content = Button(text="Invalid Pulse Operation\nI won't do it!")
        popup = Popup(content=content, auto_dismiss=False)


class TesterGUIApp(App):
    title = "DPV Tester App"

    def build(self):
        return ModuleSelect()


def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))


kivy.resources.resource_add_path(resourcePath())  # add this line

TesterGUI = TesterGUIApp()

TesterGUI.run()

if TesterGUI.root.testArea.mk_ten == None:
    pass
else:
    TesterGUI.root.testArea.mk_ten.close()
if TesterGUI.root.testArea.ard == None:
    pass
else:
    TesterGUI.root.testArea.ard.close()
if TesterGUI.root.testArea.isaac == None:
    pass
else:
    TesterGUI.root.testArea.isaac.close()
if TesterGUI.root.testArea.entris == None:
    pass
else:
    TesterGUI.root.testArea.entris.close()
